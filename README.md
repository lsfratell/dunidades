## Roadmap
- [ ] Gestores
- [x] Grupos
- [ ] Unidades
    - [x] Cadastro
    - [ ] Ações
    - [ ] Relatórios
- [x] Faturas
    - [x] Produtos
    - [x] Compensações / Devoluções
    - [x] Tarifas
    - [x] Impostos
    - [x] Cálculo de valores finais
- [ ] Concessionárias
    - [ ] Cadastro
    - [ ] Desconto

### Dependências
- Python

### Executar
No terminal, execute os seguintes comandos:
```bash
git clone http://104.238.133.84/lsfratel/dunidades.git
cd dunidades
python -m venv .venv
.\.venv\Scripts\activate
pip install -r requirements.txt
python -m dunidades
```
e a aplicação deverá executar.

### Compilação
No terminal, execute os seguintes comandos:
```bash
git clone http://104.238.133.84/lsfratel/dunidades.git
cd dunidades
python -m venv .venv
.\.venv\Scripts\activate
pip install -r requirements.txt
.\run_setup.bat
```
o executável vai estar dentro da pasta `dist/dunidades`.