import locale
import datetime
from typing import List
from mako.template import Template
from PySide2 import QtWidgets, QtCore

from dunidades.frm_browser import FrmBrowser
from dunidades.modelos import Fatura, Unidade
from dunidades.objetos import VQCheckBox
from dunidades.utilidades import paraStr, qDateFromDatetime
from dunidades.uis.ui_frm_relatorio import Ui_frmRelatorio


class FrmRelatorio(QtWidgets.QDialog, Ui_frmRelatorio):

    def __init__(self, parent=None, unidade=None):
        super(FrmRelatorio, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
        self.unidade: Unidade = unidade
        self.faturas: List[Fatura] = None
        self.conectarSinais()
        self.configurar()

    def conectarSinais(self):
        self.pbAnalisar.clicked.connect(self.analisarUnidade)
        self.ckDem1.stateChanged.connect(self.ckDemFat1.setEnabled)
        self.ckDem2.stateChanged.connect(self.ckDemFat2.setEnabled)
        self.ckDem3.stateChanged.connect(self.ckDemFat3.setEnabled)
        self.pbGerar.clicked.connect(self.gerarRelatorioDeEconomiaVerdeSazonal)

    def limparFormulario(self):
        ck: VQCheckBox
        for ck in self.findChildren(VQCheckBox):
            ck.setText('CheckBox')
            ck.setChecked(True)
            ck.setVisible(False)
            ck.setProperty('valor', 0)
            ck.setProperty('mes', QtCore.QDate())

    def gerarRelatorioDeEconomiaVerdeSazonal(self):
        u_demanda_c = 0
        u_demanda_total = 0
        comp_total = 0
        d_complementar_total = 0
        d_complementar_f_total = 0
        d_valor_total = 0
        dc_valor_total = 0
        ufer_dmcr_total = 0
        consumo_total = 0
        valor_fatura_total = 0
        demandas = []
        demandas_t = []
        compensacoes = []

        ck: VQCheckBox
        for ck in self.gbUltrapassagem.findChildren(VQCheckBox):
            if ck.isVisible() and ck.isChecked():
                u_demanda_c += ck.property('valor')
        for ck in self.gbCompensacoes.findChildren(VQCheckBox):
            if ck.isVisible() and ck.isChecked():
                ft: Fatura = self.obterFatura(ck.property('id'))
                comp_total += ft.vs_comp_valor
                compensacoes.append({'mes_referencia': ft.mes_referencia, 'descricao': ft.vs_comp_desc, 'valor': ft.vs_comp_valor})
        for ck in self.gbDemandaComplementar.findChildren(VQCheckBox):
            ft: Fatura
            if ck.isVisible() and ck.isChecked() and ck.isEnabled():
                ft = self.obterFatura(ck.property('id'))
                ft.m_seria = True
                d_complementar_total += ft.vs_valor_d_complementar
        for ck in self.gbDemandaComplementarFaturada.findChildren(VQCheckBox):
            ft: Fatura
            if ck.isVisible() and ck.isChecked() and ck.isEnabled():
                ft = self.obterFatura(ck.property('id'))
                ft.f_seria = True
                d_complementar_f_total += ft.vs_valor_d_complementar
        ult_d = 0
        for ft in self.faturas:
            u_demanda_total += ft.vs_valor_ultra
            d_valor_total += ft.vs_valor_demanda
            dc_valor_total += ft.vs_d_complementar
            ufer_dmcr_total += ft.vs_ufer_dmcr
            consumo_total += ft.vs_consumo_total
            valor_fatura_total += ft.valor
            if ult_d != ft.demanda_fp:
                demandas.append(paraStr(ft.demanda_fp))
                demandas_t.append(paraStr(ft.demanda_fp * 1.05))
                ult_d = ft.demanda_fp
        economia_acoes = d_complementar_total - d_complementar_f_total - u_demanda_total
        economia_total = economia_acoes + comp_total

        t = QtCore.QFile(':/verde_sazonal')
        t.open(QtCore.QIODevice.ReadOnly | QtCore.QIODevice.Text)
        st = QtCore.QTextStream(t)
        st.setCodec('UTF-8')
        template = Template(st.readAll())
        t.close()

        html = template.render(
            faturas=self.faturas,
            compensacoes=compensacoes,
            demandas='/'.join(demandas),
            demandasT='/'.join(demandas_t),
            comp_total=comp_total,
            u_demanda_total=u_demanda_total,
            d_valor_total=d_valor_total,
            dc_valor_total=dc_valor_total,
            d_complementar_f_total=d_complementar_f_total,
            d_complementar_total=d_complementar_total,
            ufer_dmcr_total=ufer_dmcr_total,
            consumo_total=consumo_total,
            valor_fatura_total=valor_fatura_total,
            economia_acoes=economia_acoes,
            economia_total=economia_total,
            observacoes=self.teObservacoes.toPlainText())

        view = FrmBrowser(self, 'Relatório de economia Verde Sazonal', html)
        view.show()

    def obterFatura(self, id):
        for ft in self.faturas:
            if ft.id == id:
                return ft

    def analisarUnidade(self):
        self.limparFormulario()
        faturas: List[Fatura] = []
        for f in self.unidade.faturas:
            di = (datetime.datetime.strptime(self.cbDataInicio.currentText(), '%b/%Y')).date()
            df = (datetime.datetime.strptime(self.cbDataFinal.currentText(), '%b/%Y')).date()
            if f.mes_referencia >= di and f.mes_referencia <= df:
                faturas.append(f)
        kf = 1
        kc = 1
        ck: VQCheckBox
        ft: Fatura
        for ft in faturas:
            # Ultrapassagem de demanda
            if ft.vs_valor_ultra:
                ck: VQCheckBox = getattr(self, f'ckUltr{kf}')
                ck.setText(f'{ft.mes_referencia.strftime("%b/%Y")}: {locale.currency(ft.vs_valor_ultra, grouping=True)}')
                ck.setProperty('mes', qDateFromDatetime(ft.mes_referencia))
                ck.setProperty('valor', ft.vs_valor_ultra)
                ck.setVisible(True)
                kf += 1
            # Compensações / Devoluções
            if ft.vs_comp_valor > 0:
                ck: VQCheckBox = getattr(self, f'ckDev{kc}')
                ck.setText(f'{ft.mes_referencia.strftime("%b/%Y")}: {locale.currency(ft.vs_comp_valor, grouping=True)}')
                ck.setProperty('mes', qDateFromDatetime(ft.mes_referencia))
                ck.setProperty('id', ft.id)
                ck.setVisible(True)
                kc += 1
        # Demanda Complementar
        if len(faturas) >= 3:
            d_seria = []
            d_atingida = 0
            for ft in faturas:
                if ft.vs_d_complementar:
                    d_seria.append(ft)
                else:
                    d_atingida += 1
            d_seria = sorted(d_seria, key=lambda ft: ft.vs_d_complementar, reverse=True)
            kr = 1
            for i in range(3):
                ft = d_seria[i]
                ft.vs_valor_d_complementar = ft.vs_d_complementar * faturas[-1].t_demanda_fp
                ck: VQCheckBox = getattr(self, f'ckDem{kr}')
                ck.setText(f'{ft.mes_referencia.strftime("%b/%Y")}: {locale.currency(ft.vs_valor_d_complementar, grouping=True)}')
                ck.setProperty('id', ft.id)
                ck.setVisible(True)
                if d_atingida >= 0 and d_atingida <= 3:
                    ck: VQCheckBox = getattr(self, f'ckDemFat{kr}')
                    ck.setText(f'{ft.mes_referencia.strftime("%b/%Y")}: {locale.currency(ft.vs_valor_d_complementar, grouping=True)}')
                    ck.setChecked(False)
                    ck.setProperty('id', ft.id)
                    ck.setVisible(True)
                kr += 1
        self.faturas = faturas

    def configurar(self):
        self.deInicioMonitoramento.setDate(qDateFromDatetime(self.unidade.inicio_monitoramento))
        for f in self.unidade.faturas:
            self.cbDataInicio.addItem(f.mes_referencia.strftime('%b/%Y'))
            self.cbDataFinal.addItem(f.mes_referencia.strftime('%b/%Y'))