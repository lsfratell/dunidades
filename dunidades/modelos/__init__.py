import locale
import datetime
from peewee import *

from dunidades.utilidades import maior3Numero

db = SqliteDatabase('database.db3')


class BaseModel(Model):

    class Meta:
        database = db


class Grupo(BaseModel):
    id = AutoField()
    nome = CharField(unique=True, index=True)
    deletado_em = DateTimeField(null=True, default=None)
    cadastrado_em = DateTimeField(default=datetime.datetime.now)

    class Meta:
        table_name = 'grupos'


class Unidade(BaseModel):
    id = AutoField()
    grupo = ForeignKeyField(Grupo, backref='unidades')
    nome = CharField(index=True)
    endereco = CharField(null=True)
    concessionaria = CharField(index=True)
    tensao_nominal = CharField(index=True)
    numero_uc = CharField(index=True)
    numero_medidor = CharField(null=True)
    codigo_cliente = CharField(null=True)
    inicio_monitoramento = DateField()
    tipo_cliente = CharField(index=True)
    modalidade = CharField(index=True)
    grupo_t = CharField(index=True)
    subgrupo_t = CharField(index=True)
    demanda_fp = DoubleField()
    demanda_p = DoubleField(default=0.0)
    icms = DoubleField(default=0.0)
    deletado_em = DateTimeField(null=True, default=None)
    cadastrado_em = DateField(default=datetime.datetime.now)

    class Meta:
        table_name = 'unidades'


class Imposto(BaseModel):
    id = AutoField()
    mes_referencia = DateField()
    concessionaria = CharField()
    pis = DoubleField(null=True)
    cofins = DoubleField(null=True)
    deletado_em = DateTimeField(null=True, default=None)
    cadastrado_em = DateField(default=datetime.datetime.now)

    class Meta:
        table_name = 'impostos'
        indexes = (
            (('mes_referencia', 'concessionaria'), True),
        )


class Fatura(BaseModel):
    id = AutoField()
    unidade = ForeignKeyField(Unidade, backref='faturas')
    tipo_cliente = CharField()
    modalidade = CharField()
    leitura_anterior = DateField()
    leitura_atual = DateField()
    proxima_leitura = DateField()
    dias_faturado = IntegerField()
    mes_referencia = DateField()
    vencimento = DateField()
    valor = DoubleField()
    medidor = CharField()
    apresentacao = DateField()
    demanda_fp = DoubleField()
    demanda_p = DoubleField()
    perda_transformador = DoubleField()
    p_demanda_registrada_p = DoubleField()
    p_demanda_registrada_fp = DoubleField()
    p_demanda_registrada_hr = DoubleField()
    p_demanda_faturada_p = DoubleField()
    p_demanda_faturada_fp = DoubleField()
    p_demanda_faturada_hr = DoubleField()
    p_dmcr_p = DoubleField()
    p_dmcr_fp = DoubleField()
    p_dmcr_hr = DoubleField()
    p_consumo_p = DoubleField()
    p_consumo_fp = DoubleField()
    p_consumo_hr = DoubleField()
    p_ufer_p = DoubleField()
    p_ufer_fp = DoubleField()
    p_ufer_hr = DoubleField()
    t_demanda_p = DoubleField()
    t_demanda_ultrapassagem_p = DoubleField()
    t_dmcr_p = DoubleField()
    t_consumo_te_p = DoubleField()
    t_consumo_tusd_p = DoubleField()
    t_consumo_p = DoubleField()
    t_ufer_p = DoubleField()
    t_demanda_fp = DoubleField()
    t_demanda_ultrapassagem_fp = DoubleField()
    t_dmcr_fp = DoubleField()
    t_consumo_te_fp = DoubleField()
    t_consumo_tusd_fp = DoubleField()
    t_consumo_fp = DoubleField()
    t_ufer_fp = DoubleField()
    t_consumo_te_hr = DoubleField()
    t_consumo_tusd_hr = DoubleField()
    t_consumo_hr = DoubleField()
    t_ufer_hr = DoubleField()
    imposto = ForeignKeyField(Imposto, backref='faturas')
    i_icms = DoubleField()
    deletado_em = DateTimeField(null=True, default=None)
    cadastrado_em = DateField(default=datetime.datetime.now)

    @property
    def vs_demanda_faturada(self):
        return maior3Numero(
            self.p_demanda_faturada_p,
            self.p_demanda_faturada_fp,
            self.p_demanda_faturada_hr)

    @property
    def vs_demanda_registrada(self):
        return maior3Numero(
            self.p_demanda_registrada_p,
            self.p_demanda_registrada_fp,
            self.p_demanda_registrada_hr)

    @property
    def vs_valor_ultra(self):
        if self.vs_demanda_faturada > (self.demanda_fp * 1.05):
            return (self.vs_demanda_faturada - self.demanda_fp) * self.t_demanda_ultrapassagem_fp
        return 0

    @property
    def vs_valor_demanda(self):
        return self.vs_demanda_faturada * self.t_demanda_fp

    @property
    def vs_d_complementar(self):
        dc = self.demanda_fp - self.vs_demanda_faturada
        if dc > 0:
            return dc
        return 0

    @property
    def vs_ufer_dmcr(self):
        uferp = self.p_ufer_p * self.t_ufer_p
        uferfp = self.p_ufer_fp * self.t_ufer_fp
        uferhr = self.p_ufer_hr * self.t_ufer_hr
        dmcr = maior3Numero(self.p_dmcr_p, self.p_dmcr_fp, self.p_dmcr_hr)
        if dmcr > self.vs_demanda_faturada:
            dmcr = (dmcr - self.vs_demanda_faturada) * self.t_demanda_fp
        else:
            dmcr = 0
        return uferfp + uferhr + uferp + dmcr

    @property
    def vs_consumo_total(self):
        return self.p_consumo_fp + self.p_consumo_p + self.p_consumo_hr

    @property
    def valor_fatura(self):
        return locale.currency(self.valor, grouping=True)

    @property
    def vs_comp_desc(self):
        descs = [c.descricao for c in self.compensacoes]
        return ','.join(descs)

    @property
    def vs_comp_valor(self):
        v = 0
        for c in self.compensacoes:
            v += c.valor
        return v

    class Meta:
        table_name = 'faturas'


class Compensacao(BaseModel):
    id = AutoField()
    fatura = ForeignKeyField(Fatura, backref='compensacoes')
    descricao = CharField()
    valor = DoubleField()
    deletado_em = DateTimeField(null=True, default=None)
    cadastrado_em = DateField(default=datetime.datetime.now)

    class Meta:
        table_name = 'compensacoes'
