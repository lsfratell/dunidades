import datetime
import openpyxl
from PySide2 import QtWidgets

from dunidades.frm_grupos import FrmGrupos
from dunidades.frm_faturas import FrmFaturas
from dunidades.frm_relatorio import FrmRelatorio
from dunidades.modelos import Compensacao, Fatura, Imposto, db, Grupo, Unidade
from dunidades.uis.ui_frm_unidades import Ui_frmUnidades
from dunidades.frm_cadastro_unidade import FrmCadastroUnidade
from dunidades.utilidades import paraFloat, tableWidgetItemC, tableWidgetItemL


class FrmUnidades(QtWidgets.QMainWindow, Ui_frmUnidades):

    def __init__(self):
        super(FrmUnidades, self).__init__()
        self.setupUi(self)
        self.configurarTabela()
        self.conectarSinais()
        self.carregarUnidades()

    def conectarSinais(self):
        self.pbGrupos.clicked.connect(self.abrirFrmGrupos)
        self.pbCadastrarUnidade.clicked.connect(self.abrirFrmCadastroUnidade)
        self.pbRecarregar.clicked.connect(self.recarregarUnidades)
        self.pbDeletar.clicked.connect(self.deletarUnidadeSelecionada)
        self.pbEditar.clicked.connect(self.editarUnidadeSelecionada)
        self.pbPesquisar.clicked.connect(self.pesquisarUnidades)
        self.pbFaturas.clicked.connect(self.abrirFrmFaturas)
        self.pbGerarRelatorio.clicked.connect(self.abrirFrmRelatorio)
        self.pbImportarExcel.clicked.connect(self.importarPlanilhaClientePadrao)

    def importarPlanilhaClientePadrao(self):
        fileName: tuple = QtWidgets.QFileDialog.getOpenFileName(self, 'Importar', '', 'Excel (*.xls *.xlsm )')
        if not fileName[0]:
            return
        workbook = openpyxl.load_workbook(fileName[0])
        with db.atomic():
            grupo = Grupo.get_or_create(nome=workbook['cliente']['O2'].value)
            unidade = (Unidade
                .get_or_create(
                    grupo=grupo[0],
                    nome=workbook['cliente']['O2'].value,
                    endereco=workbook['cliente']['N2'].value,
                    concessionaria=workbook['cliente']['A2'].value,
                    tensao_nominal=workbook['cliente']['F2'].value,
                    numero_uc=workbook['cliente']['G2'].value,
                    numero_medidor=workbook['cliente']['H2'].value,
                    codigo_cliente=workbook['cliente']['M2'].value,
                    inicio_monitoramento=datetime.datetime.strptime(workbook['cliente']['P2'].value, '%b/%Y'),
                    tipo_cliente=workbook['cliente']['B2'].value.upper(),
                    modalidade=workbook['cliente']['C2'].value.upper(),
                    grupo_t=workbook['cliente']['D2'].value,
                    subgrupo_t=workbook['cliente']['E2'].value.upper(),
                    demanda_fp=float(workbook['cliente']['I2'].value),
                    demanda_p=float(workbook['cliente']['J2'].value),
                    icms=float(workbook['cliente']['L2'].value)
                ))
            for row in range(2, 1000):
                if workbook['faturas'][f'A{row}'].value is None:
                    break
                try:
                    imposto = (Imposto
                        .get_or_create(
                            mes_referencia=workbook['faturas'][f'E{row}'].value,
                            concessionaria=unidade[0].concessionaria,
                            pis=paraFloat(workbook['faturas'][f'AM{row}'].value),
                            cofins=paraFloat(workbook['faturas'][f'AN{row}'].value)
                        ))
                except:
                    imposto = [(Imposto
                        .get(
                            (Imposto.mes_referencia == workbook['faturas'][f'E{row}'].value)
                            &
                            (Imposto.concessionaria == unidade[0].concessionaria)
                        ))]

                fatura = (Fatura
                    .create(
                        unidade=unidade[0],
                        tipo_cliente=workbook['faturas'][f'AV{row}'].value.upper(),
                        modalidade=workbook['faturas'][f'AW{row}'].value.upper(),
                        leitura_anterior=datetime.datetime.strptime(workbook['faturas'][f'A{row}'].value, '%d/%m/%Y'),
                        leitura_atual=datetime.datetime.strptime(workbook['faturas'][f'B{row}'].value, '%d/%m/%Y'),
                        proxima_leitura=datetime.datetime.strptime(workbook['faturas'][f'C{row}'].value, '%d/%m/%Y'),
                        dias_faturado=int(workbook['faturas'][f'D{row}'].value),
                        mes_referencia=workbook['faturas'][f'E{row}'].value,
                        vencimento=datetime.datetime.strptime(workbook['faturas'][f'F{row}'].value, '%d/%m/%Y'),
                        valor=paraFloat(workbook['faturas'][f'G{row}'].value),
                        medidor=workbook['faturas'][f'H{row}'].value,
                        apresentacao=datetime.datetime.strptime(workbook['faturas'][f'I{row}'].value, '%d/%m/%Y'),
                        demanda_fp=float(workbook['faturas'][f'J{row}'].value),
                        demanda_p=float(workbook['faturas'][f'K{row}'].value),
                        perda_transformador=float(workbook['faturas'][f'AQ{row}'].value),
                        p_demanda_registrada_p=paraFloat(workbook['faturas'][f'M{row}'].value),
                        p_demanda_registrada_fp=paraFloat(workbook['faturas'][f'L{row}'].value),
                        p_demanda_registrada_hr=paraFloat(workbook['faturas'][f'N{row}'].value),
                        p_demanda_faturada_p=paraFloat(workbook['faturas'][f'AT{row}'].value),
                        p_demanda_faturada_fp=paraFloat(workbook['faturas'][f'AR{row}'].value),
                        p_demanda_faturada_hr=paraFloat(workbook['faturas'][f'N{row}'].value),
                        p_dmcr_p=paraFloat(workbook['faturas'][f'V{row}'].value),
                        p_dmcr_fp=paraFloat(workbook['faturas'][f'U{row}'].value),
                        p_dmcr_hr=paraFloat(workbook['faturas'][f'W{row}'].value),
                        p_consumo_p=paraFloat(workbook['faturas'][f'S{row}'].value),
                        p_consumo_fp=paraFloat(workbook['faturas'][f'R{row}'].value),
                        p_consumo_hr=paraFloat(workbook['faturas'][f'T{row}'].value),
                        p_ufer_p=paraFloat(workbook['faturas'][f'P{row}'].value),
                        p_ufer_fp=paraFloat(workbook['faturas'][f'O{row}'].value),
                        p_ufer_hr=paraFloat(workbook['faturas'][f'Q{row}'].value),
                        t_demanda_p=paraFloat(workbook['faturas'][f'AD{row}'].value),
                        t_demanda_ultrapassagem_p=paraFloat(workbook['faturas'][f'AP{row}'].value),
                        t_dmcr_p=paraFloat(workbook['faturas'][f'AU{row}'].value),
                        t_consumo_te_p=paraFloat(workbook['faturas'][f'AG{row}'].value),
                        t_consumo_tusd_p=paraFloat(workbook['faturas'][f'AF{row}'].value),
                        t_consumo_p=paraFloat(workbook['faturas'][f'AE{row}'].value),
                        t_ufer_p=paraFloat(workbook['faturas'][f'AH{row}'].value),
                        t_demanda_fp=paraFloat(workbook['faturas'][f'X{row}'].value),
                        t_demanda_ultrapassagem_fp=paraFloat(workbook['faturas'][f'AC{row}'].value),
                        t_dmcr_fp=paraFloat(workbook['faturas'][f'AS{row}'].value),
                        t_consumo_te_fp=paraFloat(workbook['faturas'][f'AA{row}'].value),
                        t_consumo_tusd_fp=paraFloat(workbook['faturas'][f'Z{row}'].value),
                        t_consumo_fp=paraFloat(workbook['faturas'][f'Y{row}'].value),
                        t_ufer_fp=paraFloat(workbook['faturas'][f'AB{row}'].value),
                        t_consumo_te_hr=paraFloat(workbook['faturas'][f'AK{row}'].value),
                        t_consumo_tusd_hr=paraFloat(workbook['faturas'][f'AJ{row}'].value),
                        t_consumo_hr=paraFloat(workbook['faturas'][f'AI{row}'].value),
                        t_ufer_hr=paraFloat(workbook['faturas'][f'AL{row}'].value),
                        i_icms=paraFloat(workbook['faturas'][f'AO{row}'].value),
                        imposto=imposto[0]
                    ))
                for row in range(1, 1000):
                    if workbook['devolucoes'][f'A{row}'].value is None:
                        break
                    dt = workbook['devolucoes'][f'A{row}'].value
                    if fatura.mes_referencia == dt:
                        try:
                            (Compensacao
                                .create(
                                    fatura=fatura,
                                    descricao=workbook['devolucoes'][f'B{row}'].value,
                                    valor=paraFloat(str(workbook['devolucoes'][f'C{row}'].value))
                                ))
                        except:
                            ...

    def abrirFrmRelatorio(self):
        ms = self.twUnidades.selectionModel()
        if ms.hasSelection():
            linha = ms.selectedRows()[0].row()
            uid = int(self.twUnidades.item(linha, 0).text())
            frm = FrmRelatorio(self, Unidade.get_by_id(uid))
            frm.show()

    def abrirFrmGrupos(self):
        frm = FrmGrupos(self)
        frm.show()

    def abrirFrmCadastroUnidade(self):
        frm = FrmCadastroUnidade(self)
        frm.show()

    def configurarTabela(self):
        header = self.twUnidades.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(5, QtWidgets.QHeaderView.ResizeToContents)

    def abrirFrmFaturas(self):
        ms = self.twUnidades.selectionModel()
        if ms.hasSelection():
            linha = ms.selectedRows()[0].row()
            uid = int(self.twUnidades.item(linha, 0).text())
            frm = FrmFaturas(self, uid)
            frm.show()

    def pesquisarUnidades(self):
        unidades = (Unidade
            .select(Unidade, Grupo)
            .join(Grupo)
            .where(
                (Unidade.deletado_em.is_null()) & (Unidade.nome.contains(self.lePesquisarTexto.text())) & (Grupo.deletado_em.is_null()))
            .order_by(Unidade.id.desc()))
        self.carregarUnidades(unidades)

    def carregarUnidades(self, unidades=None):
        if unidades is None:
            unidades = (Unidade
                .select(Unidade, Grupo)
                .join(Grupo)
                .where((Unidade.deletado_em.is_null()) & (Grupo.deletado_em.is_null()))
                .order_by(Unidade.id.desc()))
        self.twUnidades.setRowCount(len(unidades))
        for l, u in enumerate(unidades):
            self.twUnidades.setItem(l, 0, tableWidgetItemC(u.id))
            self.twUnidades.setItem(l, 1, tableWidgetItemL(u.grupo.nome))
            self.twUnidades.setItem(l, 2, tableWidgetItemL(u.nome))
            self.twUnidades.setItem(l, 3, tableWidgetItemL(u.endereco))
            self.twUnidades.setItem(l, 4, tableWidgetItemC(u.numero_uc))
            self.twUnidades.setItem(l, 5, tableWidgetItemC(u.inicio_monitoramento.strftime('%b/%Y')))

    def recarregarUnidades(self):
        self.carregarUnidades()

    def deletarUnidadeSelecionada(self):
        ms = self.twUnidades.selectionModel()
        if ms.hasSelection():
            with db.atomic():
                linha = ms.selectedRows()[0].row()
                unidade = Unidade.get_by_id(int(self.twUnidades.item(linha, 0).text()))
                unidade.deletado_em = datetime.datetime.now()
                unidade.save()
                self.recarregarUnidades()

    def editarUnidadeSelecionada(self):
        ms = self.twUnidades.selectionModel()
        if ms.hasSelection():
            linha = ms.selectedRows()[0].row()
            sid = self.twUnidades.item(linha, 0).text()
            unidade = Unidade.get_by_id(int(sid))
            frm = FrmCadastroUnidade(self, unidade)
            frm.show()
