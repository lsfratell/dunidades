import tempfile
from PySide2 import QtWidgets, QtWebEngineWidgets, QtGui, QtCore


class FrmBrowser(QtWidgets.QDialog):

    def __init__(self, parent=None, title=None, html=None):
        super(FrmBrowser, self).__init__(parent)
        self.html = html
        self.setWindowTitle(title)
        self.resize(1280, 720)
        self.view = None
        self.initUi()
        self.conectarSinais()

    def initUi(self):
        vBox = QtWidgets.QVBoxLayout(self)
        vBox.setMargin(0)
        view = QtWebEngineWidgets.QWebEngineView()
        view.setHtml(self.html)
        vBox.addWidget(view)
        self.setLayout(vBox)
        self.view = view

    def conectarSinais(self):
        QtWidgets.QShortcut(
            QtGui.QKeySequence(QtCore.Qt.CTRL + QtCore.Qt.Key_P),
            self.view, activated=self.printPreview)

    def printPreview(self):
        _, p = tempfile.mkstemp(suffix='.html')
        with open(p, 'w', encoding='utf8') as f:
            f.write(self.html)
            f.close()
        QtGui.QDesktopServices.openUrl(QtCore.QUrl.fromLocalFile(p))