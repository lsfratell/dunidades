import locale
import datetime
from PySide2 import QtWidgets, QtCore

from dunidades.modelos import Unidade, db, Fatura
from dunidades.uis.ui_frm_faturas import Ui_Dialog
from dunidades.utilidades import paraStr, tableWidgetItemC
from dunidades.frm_cadastro_fatura import FrmCadastroFatura


class FrmFaturas(QtWidgets.QDialog, Ui_Dialog):

    def __init__(self, parent=None, unidadeId=None):
        super(FrmFaturas, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
        self.configurarTabela()
        self.parent = parent
        self.unidadeId = unidadeId
        self.conectarSinais()
        self.carregarFaturas()

    def conectarSinais(self):
        self.pbCadastro.clicked.connect(self.abrirFrmCadastroFatura)
        self.pbEditar.clicked.connect(self.editarFaturaSelecionada)
        self.pbDeletar.clicked.connect(self.deletarFaturaSelecionada)
        self.pbCarregar.clicked.connect(self.carregarFaturas)

    def configurarTabela(self):
        header = self.twFaturas.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(2, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(3, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(4, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(5, QtWidgets.QHeaderView.Stretch)

    def editarFaturaSelecionada(self):
        ms = self.twFaturas.selectionModel()
        if ms.hasSelection():
            linha = ms.selectedRows()[0].row()
            fid = self.twFaturas.item(linha, 0).text()
            frm = FrmCadastroFatura(self, Fatura.get_by_id(int(fid)), Unidade.get_by_id(int(self.unidadeId)))
            frm.show()

    def deletarFaturaSelecionada(self):
        ms = self.twFaturas.selectionModel()
        if ms.hasSelection():
            linha = ms.selectedRows()[0].row()
            fid = self.twFaturas.item(linha, 0).text()
            with db.atomic():
                fatura = Fatura.get_by_id(int(fid))
                fatura.deletado_em = datetime.datetime.now()
                fatura.save()
                self.recarregarFaturas()

    def abrirFrmCadastroFatura(self):
        frm = FrmCadastroFatura(self, unidade=Unidade.get_by_id(int(self.unidadeId)))
        frm.show()

    def carregarFaturas(self):
        self.twFaturas.setRowCount(0)
        faturas = (Fatura
            .select()
            .where((Fatura.unidade == self.unidadeId) & (Fatura.deletado_em.is_null()))
            .order_by(Fatura.id.desc()))
        for f in faturas:
            row = self.twFaturas.rowCount()
            self.twFaturas.setRowCount(row + 1)
            self.twFaturas.setItem(row, 0, tableWidgetItemC(f.id))
            self.twFaturas.setItem(row, 1, tableWidgetItemC(f.mes_referencia.strftime('%b/%Y')))
            self.twFaturas.setItem(row, 2, tableWidgetItemC(f.vencimento.strftime('%b/%Y')))
            self.twFaturas.setItem(row, 3, tableWidgetItemC(paraStr(f.demanda_fp)))
            self.twFaturas.setItem(row, 4, tableWidgetItemC(paraStr(f.demanda_p)))
            self.twFaturas.setItem(row, 5, tableWidgetItemC(locale.currency(f.valor, grouping=True)))

    def recarregarFaturas(self):
        self.carregarFaturas()
