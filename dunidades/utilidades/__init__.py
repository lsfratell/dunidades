from PySide2 import QtWidgets, QtCore
from dateutil.relativedelta import relativedelta

def maior3Numero(n1, n2, n3):
    if (n1 > n2) and (n1 > n3):
        return n1
    elif (n2 > n1) and (n2 > n3):
        return n2
    else:
        return n3

def dataAddDias(data, dias):
    return data + relativedelta(days=dias)

def qDateFromDatetime(dt):
    return QtCore.QDate.fromString(dt.strftime('%Y-%m-%d'), 'yyyy-MM-dd')

def dataAddMeses(data, mes):
    return data + relativedelta(months=mes)

def dataDiferenca(data1, data2):
    return abs((data1 - data2).days)

def paraFloat(t: str):
    return float(t.replace('.', '').replace(',', '.'))

def paraStr(t: float):
    return str(t).replace('.', ',')

def tableWidgetItemL(txt):
    item = QtWidgets.QTableWidgetItem(str(txt))
    item.setTextAlignment(QtCore.Qt.AlignLeft | QtCore.Qt.AlignVCenter)
    return item

def tableWidgetItemC(txt):
    item = QtWidgets.QTableWidgetItem(str(txt))
    item.setTextAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
    return item