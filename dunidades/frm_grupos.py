import datetime
from PySide2 import QtWidgets, QtCore

from dunidades.modelos import db, Grupo
from dunidades.objetos import RQLineEdit
from dunidades.uis.ui_frm_grupos import Ui_frmGrupos
from dunidades.utilidades import tableWidgetItemC, tableWidgetItemL


class FrmGrupos(QtWidgets.QDialog, Ui_frmGrupos):

    def __init__(self, parent=None):
        super(FrmGrupos, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
        self.configurarTabela()
        self.parent = parent
        self.conectarSinais()
        self.editando = None
        self.carregarGrupos()

    def conectarSinais(self):
        self.pbSalvar.clicked.connect(self.salvarGrupo)
        self.pbLimpar.clicked.connect(self.limparFormulaio)
        self.pbPesquisar.clicked.connect(self.pesquisarGrupos)
        self.pbEditar.clicked.connect(self.editarGrupoSelecionado)
        self.pbDeletar.clicked.connect(self.deletarGrupoSelecionado)
        self.pbRecarregar.clicked.connect(self.recarregarGrupos)

    def configurarTabela(self):
        header = self.twGrupos.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.ResizeToContents)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.Stretch)

    def frmParaGrupo(self, grupo):
        with db.atomic():
            grupo.nome = self.leNome.text()
            grupo.save()

    def deletarGrupoSelecionado(self):
        ms = self.twGrupos.selectionModel()
        if ms.hasSelection():
            with db.atomic():
                linha = ms.selectedRows()[0].row()
                gid = int(self.twGrupos.item(linha, 0).text())
                grupo = Grupo.get_by_id(gid)
                grupo.deletado_em = datetime.datetime.now()
                grupo.save()
                self.recarregarGrupos()

    def editarGrupoSelecionado(self):
        ms = self.twGrupos.selectionModel()
        if ms.hasSelection():
            linha = ms.selectedRows()[0].row()
            sid = self.twGrupos.item(linha, 0).text()
            self.editando = Grupo.get_by_id(int(sid))
            self.leNome.setText(self.editando.nome)
            self.leNome.setFocus()

    def pesquisarGrupos(self):
        grupos = (Grupo
            .select()
            .where((Grupo.nome.contains(self.lePesquisarTexto.text()) & (Grupo.deletado_em.is_null())))
            .order_by(Grupo.id.desc()))
        self.carregarGrupos(grupos)

    def carregarGrupos(self, grupos=None):
        if grupos is None:
            grupos = (Grupo
                .select()
                .where(Grupo.deletado_em.is_null())
                .order_by(Grupo.id.desc()))
        self.twGrupos.setRowCount(0)
        for g in grupos:
            row = self.twGrupos.rowCount()
            self.twGrupos.setRowCount(row + 1)
            self.twGrupos.setItem(row, 0, tableWidgetItemC(g.id))
            self.twGrupos.setItem(row, 1, tableWidgetItemL(g.nome))

    def validarFormulario(self):
        ctrls = []
        for obj in self.findChildren(RQLineEdit):
            if obj.text() is None:
                ctrls.append(obj.property('name'))
        return ctrls

    def recarregarGrupos(self):
        self.carregarGrupos()

    def salvarGrupo(self):
        invalidos = self.validarFormulario()

        if len(invalidos) > 0:
            return QtWidgets.QMessageBox.information(self,
                'Campos Requereido',
                f'Os seguintes campos são requerido:<br><b>{"<br>".join(invalidos)}</b>')

        try:
            if self.editando is None:
                self.frmParaGrupo(Grupo())
            else:
                self.frmParaGrupo(self.editando)
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, 'Ocorreu um erro', str(e))
        else:
            self.limparFormulaio()
            self.recarregarGrupos()
            self.parent.recarregarUnidades()

    def limparFormulaio(self):
        for obj in self.findChildren(RQLineEdit):
            obj.setText('')
        self.editando = None