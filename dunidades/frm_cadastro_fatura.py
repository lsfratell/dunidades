import datetime
from PySide2 import QtWidgets, QtCore, QtGui

from dunidades.uis.ui_frm_cadastro_fatura import Ui_frmCadastroFatura
from dunidades.modelos import Imposto, Unidade, db, Fatura, Compensacao
from dunidades.objetos import RQComboBox, RQLineEdit
from dunidades.utilidades import (
    dataAddDias,
    dataAddMeses,
    dataDiferenca, maior3Numero,
    paraFloat,
    paraStr,
    qDateFromDatetime)


class FrmCadastroFatura(QtWidgets.QDialog, Ui_frmCadastroFatura):

    def __init__(self, parent=None, fatura=None, unidade=None):
        super(FrmCadastroFatura, self).__init__(parent)
        self.setupUi(self)
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
        self.parent = parent
        self.unidade = unidade
        self.editando = fatura
        self.conectarSinais()
        self.configurarTabela()
        self.carregarDados()

    def conectarSinais(self):
        self.pbSalvar.clicked.connect(self.salvarFatura)
        self.pbCancelar.clicked.connect(self.close)
        self.pbAdicionarCompensacao.clicked.connect(self.adicionarCompensacao)
        self.pbSomarConsumoP.clicked.connect(self.somarConsumoP)
        self.pbSomarConsumoFp.clicked.connect(self.somarConsumoFp)
        self.pbSomarConsumoHr.clicked.connect(self.somarConsumoHr)
        self.pbCalcularValores.clicked.connect(self.calcularValores)
        self.deMesReferencia.dateChanged.connect(self.carregarImposto)
        QtWidgets.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Delete), self.twCompensacoes, activated=self.deletarCompensacao)

    def configurarTabela(self):
        header = self.twCompensacoes.horizontalHeader()
        header.setSectionResizeMode(0, QtWidgets.QHeaderView.Stretch)
        header.setSectionResizeMode(1, QtWidgets.QHeaderView.ResizeToContents)

    def carregarImposto(self, qDate):
        try:
            imposto = (Imposto
                        .select()
                        .where(
                            (Imposto.concessionaria == self.unidade.concessionaria) &
                            (Imposto.mes_referencia == datetime.date(qDate.year(), qDate.month(), qDate.day())))
                        .get())
            self.leImpostoPis.setText(paraStr(imposto.pis))
            self.leImpostoCofins.setText(paraStr(imposto.cofins))
        except:
            self.leImpostoPis.setText('0,0')
            self.leImpostoCofins.setText('0,0')

    def limparValores(self):
        for ctr in self.tabValores.findChildren(QtWidgets.QLineEdit):
            ctr.setText('0,0')

    def calcularValores(self):
        self.limparValores()
        demanda_p = paraFloat(self.leDemandaContratadaP.text())
        demanda_fp = paraFloat(self.leDemandaContratadaFp.text())
        t_demanda_p = paraFloat(self.leTarifaPDemanda.text())
        t_demanda_fp = paraFloat(self.leTarifaFpDemanda.text())
        t_demanda_u_p = paraFloat(self.leTarifaPDemandaUltrapassagem.text())
        t_demanda_u_fp = paraFloat(self.leTarifaFpDemandaUltrapassagem.text())
        t_dmcr_p = paraFloat(self.leTarifaPDmcr.text())
        t_dmcr_fp = paraFloat(self.leTarifaFpDmcr.text())
        t_consumo_p = paraFloat(self.leTarifaPConsumo.text())
        t_consumo_fp = paraFloat(self.leTarifaFpConsumo.text())
        t_ufer_p = paraFloat(self.leTarifaPUfer.text())
        t_ufer_fp = paraFloat(self.leTarifaFpUfer.text())
        t_consumo_hr = paraFloat(self.leTarifaHrConsumo.text())
        t_ufer_hr = paraFloat(self.leTarifaHrUfer.text())
        p_demanda_p = paraFloat(self.leProdutosDemandaFaturadaP.text())
        p_demanda_fp = paraFloat(self.leProdutosDemandaFaturadaFp.text())
        p_demanda_hr = paraFloat(self.leProdutosDemandaFaturadaHr.text())
        p_dmcr_p = paraFloat(self.leProdutosDmcrP.text())
        p_dmcr_fp = paraFloat(self.leProdutosDmcrFp.text())
        p_dmcr_hr = paraFloat(self.leProdutosDmcrHr.text())
        p_consumo_p = paraFloat(self.leProdutosConsumoP.text())
        p_consumo_fp = paraFloat(self.leProdutosConsumoFp.text())
        p_consumo_hr = paraFloat(self.leProdutosConsumoHr.text())
        p_ufer_p = paraFloat(self.leProdutosUferP.text())
        p_ufer_fp = paraFloat(self.leProdutosUferFp.text())
        p_ufer_hr = paraFloat(self.leProdutosUferHr.text())
        self.leValoresPConsumo.setText(paraStr(p_consumo_p * t_consumo_p))
        self.leValoresFpConsumo.setText(paraStr(p_consumo_fp * t_consumo_fp))
        self.leValoresHrConsumo.setText(paraStr(p_consumo_hr * t_consumo_hr))
        self.leValoresPUfer.setText(paraStr(p_ufer_p * t_ufer_p))
        self.leValoresFpUfer.setText(paraStr(p_ufer_fp * t_ufer_fp))
        self.leValoresHrUfer.setText(paraStr(p_ufer_hr * t_ufer_hr))
        valorCompensacoes = 0.0
        for l in range(self.twCompensacoes.rowCount()):
            valorCompensacoes += paraFloat(self.twCompensacoes.item(l, 1).text())
        self.leValoresCompensacoes.setText(paraStr(valorCompensacoes))
        if p_dmcr_p > p_demanda_p:
            dmcr_u_p = p_dmcr_p - p_demanda_p
            self.leValoresPDmcr.setText(paraStr(dmcr_u_p * t_dmcr_p))
        if p_dmcr_fp > p_demanda_fp:
            dmcr_u_fp = p_dmcr_fp - p_demanda_fp
            self.leValoresFpDmcr.setText(paraStr(dmcr_u_fp * t_dmcr_fp))
        if self.cbModalidade.text() == 'VERDE':
            if self.cbTipoCliente.text() == 'SAZONAL':
                mairDemanda = maior3Numero(p_demanda_p, p_demanda_fp, p_demanda_hr)
                if mairDemanda > (demanda_fp * 1.05):
                    ultr = mairDemanda - demanda_fp
                    self.leValoresFpDemandaUltrapassagem.setText(paraStr(ultr * t_demanda_u_fp))
                self.leValoresFpDemanda.setText(paraStr(mairDemanda * t_demanda_fp))
            elif self.cbTipoCliente.text() == 'INDUSTRIAL':
                if p_demanda_fp > (demanda_fp * 1.05):
                    ultr = p_demanda_fp - demanda_fp
                    self.leValoresFpDemandaUltrapassagem.setText(paraStr(ultr * t_demanda_u_fp))
                if p_demanda_fp > demanda_fp:
                    self.leValoresFpDemanda.setText(paraStr(p_demanda_fp * t_demanda_fp))
                else:
                    self.leValoresFpDemanda.setText(paraStr(demanda_fp * t_demanda_fp))
        if self.cbModalidade.text() == 'AZUL':
            if p_demanda_fp > (demanda_fp * 1.05):
                ultr = p_demanda_fp - demanda_fp
                self.leValoresFpDemandaUltrapassagem.setText(paraStr(ultr * t_demanda_u_fp))
            if p_demanda_p > (demanda_p * 1.05):
                ultr = p_demanda_p - demanda_p
                self.leValoresPDemandaUltrapassagem.setText(paraStr(ultr * t_demanda_u_p))
            if self.cbTipoCliente.text() == 'SAZONAL':
                self.leValoresFpDemanda.setText(paraStr(p_demanda_fp * t_demanda_fp))
                self.leValoresPDemanda.setText(paraStr(p_demanda_p * t_demanda_p))
            elif self.cbTipoCliente.text() == 'INDUSTRIAL':
                if p_demanda_fp > demanda_fp:
                    self.leValoresFpDemanda.setText(paraStr(p_demanda_fp * t_demanda_fp))
                else:
                    self.leValoresFpDemanda.setText(paraStr(demanda_fp * t_demanda_fp))
                if p_demanda_p > demanda_p:
                    self.leValoresPDemanda.setText(paraStr(p_demanda_p * t_demanda_p))
                else:
                    self.leValoresPDemanda.setText(paraStr(demanda_p * t_demanda_p))

    def somarConsumoP(self):
        te = paraFloat(self.leTarifaPConsumoTe.text())
        tusd = paraFloat(self.leTarifaPConsumoTusd.text())
        self.leTarifaPConsumo.setText(paraStr(te + tusd))

    def somarConsumoHr(self):
        te = paraFloat(self.leTarifaHrConsumoTe.text())
        tusd = paraFloat(self.leTarifaHrConsumoTusd.text())
        self.leTarifaHrConsumo.setText(paraStr(te + tusd))

    def somarConsumoFp(self):
        te = paraFloat(self.leTarifaFpConsumoTe.text())
        tusd = paraFloat(self.leTarifaFpConsumoTusd.text())
        self.leTarifaFpConsumo.setText(paraStr(te + tusd))

    def deletarCompensacao(self):
        ms = self.twCompensacoes.selectionModel()
        if ms.hasSelection():
            self.twCompensacoes.removeRow(self.twCompensacoes.currentRow())            

    def validarFormulario(self):
        for obj in self.findChildren(RQLineEdit):
            if obj.text() is None:
                return False
        for obj in self.findChildren(RQComboBox):
            if obj.text() is None:
                return False
        return True

    def adicionarCompensacao(self):
        des = self.leCompensacaoDescricao.text()
        val = self.leCompensacaoValor.text()
        if des != '' and val != '':
            row = self.twCompensacoes.rowCount()
            self.twCompensacoes.setRowCount(row + 1)
            self.twCompensacoes.setItem(row, 0, QtWidgets.QTableWidgetItem(des))
            self.twCompensacoes.setItem(row, 1, QtWidgets.QTableWidgetItem(paraStr(paraFloat(val))))

    def faturaParaFrm(self):
        self.cbTipoCliente.setText(self.editando.tipo_cliente)
        self.cbModalidade.setText(self.editando.modalidade)
        self.deLeituraAnterior.setDate(qDateFromDatetime(self.editando.leitura_anterior))
        self.deLeituraAtual.setDate(qDateFromDatetime(self.editando.leitura_atual))
        self.deProximaLeitura.setDate(qDateFromDatetime(self.editando.proxima_leitura))
        self.leDiasFaturado.setText(str(self.editando.dias_faturado))
        self.deMesReferencia.setDate(qDateFromDatetime(self.editando.mes_referencia))
        self.deVencimento.setDate(qDateFromDatetime(self.editando.vencimento))
        self.leValor.setText(paraStr(self.editando.valor))
        self.leMedidor.setText(self.editando.medidor)
        self.deApresentacao.setDate(qDateFromDatetime(self.editando.apresentacao))
        self.leDemandaContratadaFp.setText(paraStr(self.editando.demanda_fp))
        self.leDemandaContratadaP.setText(paraStr(self.editando.demanda_p))
        self.lePerdaTransformador.setText(paraStr(self.editando.perda_transformador))
        self.leProdutosDemandaRegistradaP.setText(paraStr(self.editando.p_demanda_registrada_p))
        self.leProdutosDemandaRegistradaFp.setText(paraStr(self.editando.p_demanda_registrada_fp))
        self.leProdutosDemandaRegistradaHr.setText(paraStr(self.editando.p_demanda_registrada_hr))
        self.leProdutosDemandaFaturadaP.setText(paraStr(self.editando.p_demanda_faturada_p))
        self.leProdutosDemandaFaturadaFp.setText(paraStr(self.editando.p_demanda_faturada_fp))
        self.leProdutosDemandaFaturadaHr.setText(paraStr(self.editando.p_demanda_faturada_hr))
        self.leProdutosDmcrP.setText(paraStr(self.editando.p_dmcr_p))
        self.leProdutosDmcrFp.setText(paraStr(self.editando.p_dmcr_fp))
        self.leProdutosDmcrHr.setText(paraStr(self.editando.p_dmcr_hr))
        self.leProdutosConsumoP.setText(paraStr(self.editando.p_consumo_p))
        self.leProdutosConsumoFp.setText(paraStr(self.editando.p_consumo_fp))
        self.leProdutosConsumoHr.setText(paraStr(self.editando.p_consumo_hr))
        self.leProdutosUferP.setText(paraStr(self.editando.p_ufer_p))
        self.leProdutosUferFp.setText(paraStr(self.editando.p_ufer_fp))
        self.leProdutosUferHr.setText(paraStr(self.editando.p_ufer_hr))
        self.leTarifaPDemanda.setText(paraStr(self.editando.t_demanda_p))
        self.leTarifaPDemandaUltrapassagem.setText(paraStr(self.editando.t_demanda_ultrapassagem_p))
        self.leTarifaPDmcr.setText(paraStr(self.editando.t_dmcr_p))
        self.leTarifaPConsumoTe.setText(paraStr(self.editando.t_consumo_te_p))
        self.leTarifaPConsumoTusd.setText(paraStr(self.editando.t_consumo_tusd_p))
        self.leTarifaPConsumo.setText(paraStr(self.editando.t_consumo_p))
        self.leTarifaPUfer.setText(paraStr(self.editando.t_ufer_p))
        self.leTarifaFpDemanda.setText(paraStr(self.editando.t_demanda_fp))
        self.leTarifaFpDemandaUltrapassagem.setText(paraStr(self.editando.t_demanda_ultrapassagem_fp))
        self.leTarifaFpDmcr.setText(paraStr(self.editando.t_dmcr_fp))
        self.leTarifaFpConsumoTe.setText(paraStr(self.editando.t_consumo_te_fp))
        self.leTarifaFpConsumoTusd.setText(paraStr(self.editando.t_consumo_tusd_fp))
        self.leTarifaFpConsumo.setText(paraStr(self.editando.t_consumo_fp))
        self.leTarifaFpUfer.setText(paraStr(self.editando.t_ufer_fp))
        self.leTarifaHrConsumoTe.setText(paraStr(self.editando.t_consumo_te_hr))
        self.leTarifaHrConsumoTusd.setText(paraStr(self.editando.t_consumo_tusd_hr))
        self.leTarifaHrConsumo.setText(paraStr(self.editando.t_consumo_hr))
        self.leTarifaHrUfer.setText(paraStr(self.editando.t_ufer_hr))
        self.leImpostoPis.setText(paraStr(self.editando.imposto.pis))
        self.leImpostoCofins.setText(paraStr(self.editando.imposto.cofins))
        self.leImpostoIcms.setText(paraStr(self.editando.i_icms))

        for c in self.editando.compensacoes:
            row = self.twCompensacoes.rowCount()
            self.twCompensacoes.setRowCount(row + 1)
            self.twCompensacoes.setItem(row, 0, QtWidgets.QTableWidgetItem(c.descricao))
            self.twCompensacoes.setItem(row, 1, QtWidgets.QTableWidgetItem(paraStr(c.valor)))

    def frmParaFatura(self, fatura: Fatura):
        with db.atomic():
            fatura.unidade = self.unidade
            fatura.tipo_cliente = self.cbTipoCliente.text()
            fatura.modalidade = self.cbModalidade.text()
            fatura.leitura_anterior = self.deLeituraAnterior.date().toString('yyyy-MM-dd')
            fatura.leitura_atual = self.deLeituraAtual.date().toString('yyyy-MM-dd')
            fatura.proxima_leitura = self.deProximaLeitura.date().toString('yyyy-MM-dd')
            fatura.dias_faturado = int(self.leDiasFaturado.text())
            fatura.mes_referencia = self.deMesReferencia.date().toString('yyyy-MM-dd')
            fatura.vencimento = self.deVencimento.date().toString('yyyy-MM-dd')
            fatura.valor = paraFloat(self.leValor.text())
            fatura.medidor = self.leMedidor.text()
            fatura.apresentacao = self.deApresentacao.date().toString('yyyy-MM-dd')
            fatura.demanda_fp = paraFloat(self.leDemandaContratadaFp.text())
            fatura.demanda_p = paraFloat(self.leDemandaContratadaP.text())
            fatura.perda_transformador = paraFloat(self.lePerdaTransformador.text())
            fatura.p_demanda_registrada_p = paraFloat(self.leProdutosDemandaRegistradaP.text())
            fatura.p_demanda_registrada_fp = paraFloat(self.leProdutosDemandaRegistradaFp.text())
            fatura.p_demanda_registrada_hr = paraFloat(self.leProdutosDemandaRegistradaHr.text())
            fatura.p_demanda_faturada_p = paraFloat(self.leProdutosDemandaFaturadaP.text())
            fatura.p_demanda_faturada_fp = paraFloat(self.leProdutosDemandaFaturadaFp.text())
            fatura.p_demanda_faturada_hr = paraFloat(self.leProdutosDemandaFaturadaHr.text())
            fatura.p_dmcr_p = paraFloat(self.leProdutosDmcrP.text())
            fatura.p_dmcr_fp = paraFloat(self.leProdutosDmcrFp.text())
            fatura.p_dmcr_hr = paraFloat(self.leProdutosDmcrHr.text())
            fatura.p_consumo_p = paraFloat(self.leProdutosConsumoP.text())
            fatura.p_consumo_fp = paraFloat(self.leProdutosConsumoFp.text())
            fatura.p_consumo_hr = paraFloat(self.leProdutosConsumoHr.text())
            fatura.p_ufer_p = paraFloat(self.leProdutosUferP.text())
            fatura.p_ufer_fp = paraFloat(self.leProdutosUferFp.text())
            fatura.p_ufer_hr = paraFloat(self.leProdutosUferHr.text())
            fatura.t_demanda_p = paraFloat(self.leTarifaPDemanda.text())
            fatura.t_demanda_ultrapassagem_p = paraFloat(self.leTarifaPDemandaUltrapassagem.text())
            fatura.t_dmcr_p = paraFloat(self.leTarifaPDmcr.text())
            fatura.t_consumo_te_p = paraFloat(self.leTarifaPConsumoTe.text())
            fatura.t_consumo_tusd_p = paraFloat(self.leTarifaPConsumoTusd.text())
            fatura.t_consumo_p = paraFloat(self.leTarifaPConsumo.text())
            fatura.t_ufer_p = paraFloat(self.leTarifaPUfer.text())
            fatura.t_demanda_fp = paraFloat(self.leTarifaFpDemanda.text())
            fatura.t_demanda_ultrapassagem_fp = paraFloat(self.leTarifaFpDemandaUltrapassagem.text())
            fatura.t_dmcr_fp = paraFloat(self.leTarifaFpDmcr.text())
            fatura.t_consumo_te_fp = paraFloat(self.leTarifaFpConsumoTe.text())
            fatura.t_consumo_tusd_fp = paraFloat(self.leTarifaFpConsumoTusd.text())
            fatura.t_consumo_fp = paraFloat(self.leTarifaFpConsumo.text())
            fatura.t_ufer_fp = paraFloat(self.leTarifaFpUfer.text())
            fatura.t_consumo_te_hr = paraFloat(self.leTarifaHrConsumoTe.text())
            fatura.t_consumo_tusd_hr = paraFloat(self.leTarifaHrConsumoTusd.text())
            fatura.t_consumo_hr = paraFloat(self.leTarifaHrConsumo.text())
            fatura.t_ufer_hr = paraFloat(self.leTarifaHrUfer.text())
            pis = paraFloat(self.leImpostoPis.text())
            cofins = paraFloat(self.leImpostoCofins.text())
            fatura.i_icms = paraFloat(self.leImpostoIcms.text())

            qDate = self.deMesReferencia.date()
            try:
                imposto = (Imposto
                    .select()
                    .where(
                        (Imposto.concessionaria == self.unidade.concessionaria) &
                        (Imposto.mes_referencia == datetime.date(qDate.year(), qDate.month(), qDate.day())))
                    .get())
            except:
                imposto = (Imposto
                    .create(
                        concessionaria=self.unidade.concessionaria,
                        mes_referencia=self.deMesReferencia.date().toString('yyyy-MM-dd'),
                        pis=pis,
                        cofins=cofins))
            finally:
                fatura.imposto = imposto

            fatura.save()

            (Compensacao
                .delete()
                .where(Compensacao.fatura == fatura)
                .execute())

            compensacoes = []
            for l in range(self.twCompensacoes.rowCount()):
                compensacoes.append({
                    'fatura': fatura,
                    'descricao': self.twCompensacoes.item(l, 0).text(),
                    'valor': paraFloat(self.twCompensacoes.item(l, 1).text())
                })

            (Compensacao
                .insert_many(compensacoes)
                .execute())

    def salvarFatura(self):
        if not self.validarFormulario():
            return QtWidgets.QMessageBox.information(self,
                'Campos Requeridos',
                f'<b>Todos</b> os campos são requeridos.')
        try:
            self.frmParaFatura(Fatura() if self.editando is None else self.editando)
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, 'Ocorreu um erro', str(e))
        else:
            self.parent.recarregarFaturas()
            self.close()

    def carregarDados(self):
        self.tabWidget.setCurrentIndex(0)
        if self.editando is not None:
            return self.faturaParaFrm()
        try:
            ultimaFatura = (Fatura
                .select()
                .join(Unidade)
                .where((Fatura.deletado_em.is_null()) & (Unidade.id == self.unidade.id))
                .order_by(Fatura.id.desc())
                .get())
        except:
            pass
        else:
            self.cbTipoCliente.setText(ultimaFatura.tipo_cliente)
            self.cbModalidade.setText(ultimaFatura.modalidade)
            self.deLeituraAnterior.setDate(qDateFromDatetime(ultimaFatura.leitura_atual))
            self.deLeituraAtual.setDate(qDateFromDatetime(ultimaFatura.proxima_leitura))
            self.deProximaLeitura.setDate(qDateFromDatetime(dataAddDias(ultimaFatura.proxima_leitura, 30)))
            self.leDiasFaturado.setText(str(dataDiferenca(ultimaFatura.leitura_atual, ultimaFatura.proxima_leitura)))
            self.deMesReferencia.setDate(qDateFromDatetime(dataAddMeses(ultimaFatura.mes_referencia, 1)))
            self.leDemandaContratadaFp.setText(paraStr(ultimaFatura.demanda_fp))
            self.leDemandaContratadaP.setText(paraStr(ultimaFatura.demanda_p))
            self.lePerdaTransformador.setText(paraStr(ultimaFatura.perda_transformador))
            self.leMedidor.setText(ultimaFatura.medidor)