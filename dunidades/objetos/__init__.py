from PySide2 import QtCore, QtWidgets


class RQLineEdit(QtWidgets.QLineEdit):

    def __init__(self, parent):
        super(RQLineEdit, self).__init__(parent)

    def text(self):
        text = super(RQLineEdit, self).text()
        return None if text == '' else text

    @QtCore.Property(bool)
    def requerido(self):
        return True


class NQLineEdit(QtWidgets.QLineEdit):

    def __init__(self, parent):
        super(NQLineEdit, self).__init__(parent)

    def text(self):
        text = super(NQLineEdit, self).text()
        return None if text == '' else text


class RQComboBox(QtWidgets.QComboBox):

    def __init__(self, parent):
        super(RQComboBox, self).__init__(parent)

    def text(self):
        text = self.currentText()
        return None if text == '' else text

    def setText(self, text):
        self.setCurrentText(text)

    @QtCore.Property(bool)
    def requerido(self):
        return True


class VQCheckBox(QtWidgets.QCheckBox):

    def __init__(self, parent):
        super(VQCheckBox, self).__init__(parent)
        self.setVisible(False)