from PySide2 import QtWidgets, QtCore

from dunidades.modelos import db, Grupo, Unidade
from dunidades.objetos import RQComboBox, RQLineEdit, NQLineEdit
from dunidades.utilidades import paraFloat, paraStr, qDateFromDatetime
from dunidades.uis.ui_frm_cadastro_unidade import Ui_frmCadastroUnidade


class FrmCadastroUnidade(QtWidgets.QDialog, Ui_frmCadastroUnidade):

    def __init__(self, parent=None, unidade=None):
        super(FrmCadastroUnidade, self).__init__(parent)
        self.setupUi(self)
        self.parent = parent
        self.setWindowFlags(self.windowFlags() & ~QtCore.Qt.WindowContextHelpButtonHint)
        self.conectarSinais()
        self.editando = unidade
        self.carregarDados()

    def conectarSinais(self):
        self.pbSalvar.clicked.connect(self.salvarUnidade)
        self.pbCancelar.clicked.connect(self.fecharELimpar)

    def fecharELimpar(self):
        self.limparFormulaio()
        self.close()

    def frmParaUnidade(self, unidade):
        with db.atomic():
            grupo = (Grupo
                .select()
                .where(Grupo.deletado_em.is_null())
                .get())
            unidade.grupo = grupo
            unidade.nome = self.leNome.text()
            unidade.endereco = self.leEndereco.text()
            unidade.concessionaria = self.cbConcessionaria.text()
            unidade.tensao_nominal = self.cbTensaoNominal.text()
            unidade.numero_uc = self.leNumeroUc.text()
            unidade.numero_medidor = self.leNumeroMedidor.text()
            unidade.codigo_cliente = self.leCodigoCliente.text()
            unidade.inicio_monitoramento = self.deInicioMonitoramento.date().toString('yyyy-MM-dd')
            unidade.tipo_cliente = self.cbTipoCliente.text()
            unidade.modalidade = self.cbModalidade.text()
            unidade.grupo_t = self.cbGrupoT.text()
            unidade.subgrupo_t = self.cbSubgrupoT.text()
            unidade.demanda_fp = paraFloat(self.leDemandaFp.text())
            unidade.demanda_p = paraFloat(self.leDemandaP.text())
            unidade.icms = paraFloat(self.leIcms.text())
            unidade.save()

    def unidadeParaFrm(self):
        self.cbGrupo.setText(self.editando.grupo.nome)
        self.leNome.setText(self.editando.nome)
        self.leEndereco.setText(self.editando.endereco)
        self.cbConcessionaria.setText(self.editando.concessionaria)
        self.cbTensaoNominal.setText(self.editando.tensao_nominal)
        self.leNumeroUc.setText(self.editando.numero_uc)
        self.leNumeroMedidor.setText(self.editando.numero_medidor)
        self.leCodigoCliente.setText(self.editando.codigo_cliente)
        self.deInicioMonitoramento.setDate(qDateFromDatetime(self.editando.inicio_monitoramento))
        self.cbTipoCliente.setText(self.editando.tipo_cliente)
        self.cbModalidade.setText(self.editando.modalidade)
        self.cbGrupoT.setText(self.editando.grupo_t)
        self.cbSubgrupoT.setText(self.editando.subgrupo_t)
        self.leDemandaFp.setText(paraStr(self.editando.demanda_fp))
        self.leDemandaP.setText(paraStr(self.editando.demanda_p))
        self.leIcms.setText(paraStr(self.editando.icms))

    def carregarDados(self):
        grupos = (Grupo
            .select()
            .where(Grupo.deletado_em.is_null())
            .order_by(Grupo.id.desc()))
        for g in grupos:
            self.cbGrupo.addItem(g.nome)
        if self.editando is not None:
            self.unidadeParaFrm()

    def validarFormulario(self):
        ctrls = []
        for obj in self.findChildren(RQLineEdit):
            if obj.text() is None:
                ctrls.append(obj.property('name'))
        for obj in self.findChildren(RQComboBox):
            if obj.text() is None:
                ctrls.append(obj.property('name'))
        return ctrls

    def salvarUnidade(self):
        invalidos = self.validarFormulario()

        if len(invalidos) > 0:
            return QtWidgets.QMessageBox.information(self,
                'Campos Requereido',
                f'Os seguintes campos são requerido:<br><b>{"<br>".join(invalidos)}</b>')

        try:
            self.frmParaUnidade(Unidade() if self.editando is None else self.editando)
        except Exception as e:
            QtWidgets.QMessageBox.critical(self, 'Ocorreu um erro', str(e))
        else:
            self.limparFormulaio()
            self.parent.recarregarUnidades()
            self.close()

    def limparFormulaio(self):
        for obj in self.findChildren(RQLineEdit):
            obj.setText('')
        for obj in self.findChildren(RQComboBox):
            obj.setText('')
        for obj in self.findChildren(NQLineEdit):
            obj.setText('')
        self.editando = None