# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_frm_cadastro_unidade.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from dunidades.objetos import RQLineEdit
from dunidades.objetos import RQComboBox
from dunidades.objetos import NQLineEdit


class Ui_frmCadastroUnidade(object):
    def setupUi(self, frmCadastroUnidade):
        if not frmCadastroUnidade.objectName():
            frmCadastroUnidade.setObjectName(u"frmCadastroUnidade")
        frmCadastroUnidade.resize(381, 438)
        self.verticalLayout = QVBoxLayout(frmCadastroUnidade)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_16 = QLabel(frmCadastroUnidade)
        self.label_16.setObjectName(u"label_16")

        self.gridLayout.addWidget(self.label_16, 0, 0, 1, 1)

        self.label = QLabel(frmCadastroUnidade)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 0, 1, 1, 1)

        self.cbGrupo = RQComboBox(frmCadastroUnidade)
        self.cbGrupo.setObjectName(u"cbGrupo")

        self.gridLayout.addWidget(self.cbGrupo, 1, 0, 1, 1)

        self.leNome = RQLineEdit(frmCadastroUnidade)
        self.leNome.setObjectName(u"leNome")

        self.gridLayout.addWidget(self.leNome, 1, 1, 1, 1)

        self.label_2 = QLabel(frmCadastroUnidade)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 2, 0, 1, 1)

        self.label_6 = QLabel(frmCadastroUnidade)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout.addWidget(self.label_6, 2, 1, 1, 1)

        self.leEndereco = NQLineEdit(frmCadastroUnidade)
        self.leEndereco.setObjectName(u"leEndereco")

        self.gridLayout.addWidget(self.leEndereco, 3, 0, 1, 1)

        self.cbConcessionaria = RQComboBox(frmCadastroUnidade)
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.addItem("")
        self.cbConcessionaria.setObjectName(u"cbConcessionaria")

        self.gridLayout.addWidget(self.cbConcessionaria, 3, 1, 1, 1)

        self.label_7 = QLabel(frmCadastroUnidade)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout.addWidget(self.label_7, 4, 0, 1, 1)

        self.label_3 = QLabel(frmCadastroUnidade)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 4, 1, 1, 1)

        self.cbTensaoNominal = RQComboBox(frmCadastroUnidade)
        self.cbTensaoNominal.addItem("")
        self.cbTensaoNominal.addItem("")
        self.cbTensaoNominal.setObjectName(u"cbTensaoNominal")

        self.gridLayout.addWidget(self.cbTensaoNominal, 5, 0, 1, 1)

        self.leNumeroUc = RQLineEdit(frmCadastroUnidade)
        self.leNumeroUc.setObjectName(u"leNumeroUc")

        self.gridLayout.addWidget(self.leNumeroUc, 5, 1, 1, 1)

        self.label_4 = QLabel(frmCadastroUnidade)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout.addWidget(self.label_4, 6, 0, 1, 1)

        self.label_5 = QLabel(frmCadastroUnidade)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout.addWidget(self.label_5, 6, 1, 1, 1)

        self.leNumeroMedidor = NQLineEdit(frmCadastroUnidade)
        self.leNumeroMedidor.setObjectName(u"leNumeroMedidor")

        self.gridLayout.addWidget(self.leNumeroMedidor, 7, 0, 1, 1)

        self.leCodigoCliente = NQLineEdit(frmCadastroUnidade)
        self.leCodigoCliente.setObjectName(u"leCodigoCliente")

        self.gridLayout.addWidget(self.leCodigoCliente, 7, 1, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)

        self.groupBox_2 = QGroupBox(frmCadastroUnidade)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout_2 = QGridLayout(self.groupBox_2)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_15 = QLabel(self.groupBox_2)
        self.label_15.setObjectName(u"label_15")

        self.gridLayout_2.addWidget(self.label_15, 0, 0, 1, 1)

        self.label_8 = QLabel(self.groupBox_2)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout_2.addWidget(self.label_8, 0, 1, 1, 1)

        self.deInicioMonitoramento = QDateEdit(self.groupBox_2)
        self.deInicioMonitoramento.setObjectName(u"deInicioMonitoramento")
        self.deInicioMonitoramento.setDateTime(QDateTime(QDate(2015, 1, 1), QTime(0, 0, 0)))
        self.deInicioMonitoramento.setCalendarPopup(True)

        self.gridLayout_2.addWidget(self.deInicioMonitoramento, 1, 0, 1, 1)

        self.cbTipoCliente = RQComboBox(self.groupBox_2)
        self.cbTipoCliente.addItem("")
        self.cbTipoCliente.addItem("")
        self.cbTipoCliente.setObjectName(u"cbTipoCliente")

        self.gridLayout_2.addWidget(self.cbTipoCliente, 1, 1, 1, 1)

        self.label_9 = QLabel(self.groupBox_2)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout_2.addWidget(self.label_9, 2, 0, 1, 1)

        self.label_10 = QLabel(self.groupBox_2)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout_2.addWidget(self.label_10, 2, 1, 1, 1)

        self.cbModalidade = RQComboBox(self.groupBox_2)
        self.cbModalidade.addItem("")
        self.cbModalidade.addItem("")
        self.cbModalidade.setObjectName(u"cbModalidade")

        self.gridLayout_2.addWidget(self.cbModalidade, 3, 0, 1, 1)

        self.cbGrupoT = RQComboBox(self.groupBox_2)
        self.cbGrupoT.addItem("")
        self.cbGrupoT.addItem("")
        self.cbGrupoT.setObjectName(u"cbGrupoT")

        self.gridLayout_2.addWidget(self.cbGrupoT, 3, 1, 1, 1)

        self.label_11 = QLabel(self.groupBox_2)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_2.addWidget(self.label_11, 4, 0, 1, 1)

        self.label_12 = QLabel(self.groupBox_2)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout_2.addWidget(self.label_12, 4, 1, 1, 1)

        self.cbSubgrupoT = RQComboBox(self.groupBox_2)
        self.cbSubgrupoT.addItem("")
        self.cbSubgrupoT.addItem("")
        self.cbSubgrupoT.setObjectName(u"cbSubgrupoT")

        self.gridLayout_2.addWidget(self.cbSubgrupoT, 5, 0, 1, 1)

        self.leDemandaFp = RQLineEdit(self.groupBox_2)
        self.leDemandaFp.setObjectName(u"leDemandaFp")

        self.gridLayout_2.addWidget(self.leDemandaFp, 5, 1, 1, 1)

        self.label_13 = QLabel(self.groupBox_2)
        self.label_13.setObjectName(u"label_13")

        self.gridLayout_2.addWidget(self.label_13, 6, 0, 1, 1)

        self.label_14 = QLabel(self.groupBox_2)
        self.label_14.setObjectName(u"label_14")

        self.gridLayout_2.addWidget(self.label_14, 6, 1, 1, 1)

        self.leDemandaP = RQLineEdit(self.groupBox_2)
        self.leDemandaP.setObjectName(u"leDemandaP")

        self.gridLayout_2.addWidget(self.leDemandaP, 7, 0, 1, 1)

        self.leIcms = RQLineEdit(self.groupBox_2)
        self.leIcms.setObjectName(u"leIcms")

        self.gridLayout_2.addWidget(self.leIcms, 7, 1, 1, 1)


        self.verticalLayout.addWidget(self.groupBox_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pbCancelar = QPushButton(frmCadastroUnidade)
        self.pbCancelar.setObjectName(u"pbCancelar")
        self.pbCancelar.setAutoDefault(False)

        self.horizontalLayout.addWidget(self.pbCancelar)

        self.pbSalvar = QPushButton(frmCadastroUnidade)
        self.pbSalvar.setObjectName(u"pbSalvar")
        self.pbSalvar.setAutoDefault(False)

        self.horizontalLayout.addWidget(self.pbSalvar)


        self.verticalLayout.addLayout(self.horizontalLayout)

        QWidget.setTabOrder(self.cbGrupo, self.leNome)
        QWidget.setTabOrder(self.leNome, self.leEndereco)
        QWidget.setTabOrder(self.leEndereco, self.cbConcessionaria)
        QWidget.setTabOrder(self.cbConcessionaria, self.cbTensaoNominal)
        QWidget.setTabOrder(self.cbTensaoNominal, self.leNumeroUc)
        QWidget.setTabOrder(self.leNumeroUc, self.leNumeroMedidor)
        QWidget.setTabOrder(self.leNumeroMedidor, self.leCodigoCliente)
        QWidget.setTabOrder(self.leCodigoCliente, self.deInicioMonitoramento)
        QWidget.setTabOrder(self.deInicioMonitoramento, self.cbTipoCliente)
        QWidget.setTabOrder(self.cbTipoCliente, self.cbModalidade)
        QWidget.setTabOrder(self.cbModalidade, self.cbGrupoT)
        QWidget.setTabOrder(self.cbGrupoT, self.cbSubgrupoT)
        QWidget.setTabOrder(self.cbSubgrupoT, self.leDemandaFp)
        QWidget.setTabOrder(self.leDemandaFp, self.leDemandaP)
        QWidget.setTabOrder(self.leDemandaP, self.leIcms)
        QWidget.setTabOrder(self.leIcms, self.pbCancelar)
        QWidget.setTabOrder(self.pbCancelar, self.pbSalvar)

        self.retranslateUi(frmCadastroUnidade)

        self.cbConcessionaria.setCurrentIndex(-1)
        self.cbTensaoNominal.setCurrentIndex(-1)
        self.cbTipoCliente.setCurrentIndex(-1)
        self.cbModalidade.setCurrentIndex(-1)
        self.cbGrupoT.setCurrentIndex(-1)
        self.cbSubgrupoT.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(frmCadastroUnidade)
    # setupUi

    def retranslateUi(self, frmCadastroUnidade):
        frmCadastroUnidade.setWindowTitle(QCoreApplication.translate("frmCadastroUnidade", u"Formul\u00e1rio Unidade", None))
        self.label_16.setText(QCoreApplication.translate("frmCadastroUnidade", u"Grupo:", None))
        self.label.setText(QCoreApplication.translate("frmCadastroUnidade", u"Nome:", None))
        self.cbGrupo.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Grupo", None))
        self.leNome.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Nome", None))
        self.label_2.setText(QCoreApplication.translate("frmCadastroUnidade", u"Endere\u00e7o:", None))
        self.label_6.setText(QCoreApplication.translate("frmCadastroUnidade", u"Concession\u00e1ria:", None))
        self.leEndereco.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Endere\u00e7o", None))
        self.cbConcessionaria.setItemText(0, QCoreApplication.translate("frmCadastroUnidade", u"AME", None))
        self.cbConcessionaria.setItemText(1, QCoreApplication.translate("frmCadastroUnidade", u"BOA VISTA", None))
        self.cbConcessionaria.setItemText(2, QCoreApplication.translate("frmCadastroUnidade", u"CAIU\u00c1", None))
        self.cbConcessionaria.setItemText(3, QCoreApplication.translate("frmCadastroUnidade", u"CASTRO\u00a0-\u00a0DIS", None))
        self.cbConcessionaria.setItemText(4, QCoreApplication.translate("frmCadastroUnidade", u"CEA", None))
        self.cbConcessionaria.setItemText(5, QCoreApplication.translate("frmCadastroUnidade", u"CEBDIS", None))
        self.cbConcessionaria.setItemText(6, QCoreApplication.translate("frmCadastroUnidade", u"CEDRAP", None))
        self.cbConcessionaria.setItemText(7, QCoreApplication.translate("frmCadastroUnidade", u"CEDRI", None))
        self.cbConcessionaria.setItemText(8, QCoreApplication.translate("frmCadastroUnidade", u"CEEE-D", None))
        self.cbConcessionaria.setItemText(9, QCoreApplication.translate("frmCadastroUnidade", u"CEGERO", None))
        self.cbConcessionaria.setItemText(10, QCoreApplication.translate("frmCadastroUnidade", u"CEJAMA", None))
        self.cbConcessionaria.setItemText(11, QCoreApplication.translate("frmCadastroUnidade", u"CELESC-DIS", None))
        self.cbConcessionaria.setItemText(12, QCoreApplication.translate("frmCadastroUnidade", u"CELG-D", None))
        self.cbConcessionaria.setItemText(13, QCoreApplication.translate("frmCadastroUnidade", u"CELPE", None))
        self.cbConcessionaria.setItemText(14, QCoreApplication.translate("frmCadastroUnidade", u"CEMAR", None))
        self.cbConcessionaria.setItemText(15, QCoreApplication.translate("frmCadastroUnidade", u"CEMIG-D", None))
        self.cbConcessionaria.setItemText(16, QCoreApplication.translate("frmCadastroUnidade", u"CEMIRIM", None))
        self.cbConcessionaria.setItemText(17, QCoreApplication.translate("frmCadastroUnidade", u"CEPRAG", None))
        self.cbConcessionaria.setItemText(18, QCoreApplication.translate("frmCadastroUnidade", u"CERA\u00c7\u00c1", None))
        self.cbConcessionaria.setItemText(19, QCoreApplication.translate("frmCadastroUnidade", u"CERAL", None))
        self.cbConcessionaria.setItemText(20, QCoreApplication.translate("frmCadastroUnidade", u"CERAL", None))
        self.cbConcessionaria.setItemText(21, QCoreApplication.translate("frmCadastroUnidade", u"CERAL\u00a0ARARUAMA", None))
        self.cbConcessionaria.setItemText(22, QCoreApplication.translate("frmCadastroUnidade", u"CERAL\u00a0ARARUAMA", None))
        self.cbConcessionaria.setItemText(23, QCoreApplication.translate("frmCadastroUnidade", u"CERAL\u00a0DIS", None))
        self.cbConcessionaria.setItemText(24, QCoreApplication.translate("frmCadastroUnidade", u"CERBRANORTE", None))
        self.cbConcessionaria.setItemText(25, QCoreApplication.translate("frmCadastroUnidade", u"CERCI", None))
        self.cbConcessionaria.setItemText(26, QCoreApplication.translate("frmCadastroUnidade", u"CERCOS", None))
        self.cbConcessionaria.setItemText(27, QCoreApplication.translate("frmCadastroUnidade", u"CEREJ", None))
        self.cbConcessionaria.setItemText(28, QCoreApplication.translate("frmCadastroUnidade", u"CERES", None))
        self.cbConcessionaria.setItemText(29, QCoreApplication.translate("frmCadastroUnidade", u"CERFOX", None))
        self.cbConcessionaria.setItemText(30, QCoreApplication.translate("frmCadastroUnidade", u"CERGAL", None))
        self.cbConcessionaria.setItemText(31, QCoreApplication.translate("frmCadastroUnidade", u"CERGAPA", None))
        self.cbConcessionaria.setItemText(32, QCoreApplication.translate("frmCadastroUnidade", u"CERGRAL", None))
        self.cbConcessionaria.setItemText(33, QCoreApplication.translate("frmCadastroUnidade", u"CERILUZ", None))
        self.cbConcessionaria.setItemText(34, QCoreApplication.translate("frmCadastroUnidade", u"CERIM", None))
        self.cbConcessionaria.setItemText(35, QCoreApplication.translate("frmCadastroUnidade", u"CERIPA", None))
        self.cbConcessionaria.setItemText(36, QCoreApplication.translate("frmCadastroUnidade", u"CERIS", None))
        self.cbConcessionaria.setItemText(37, QCoreApplication.translate("frmCadastroUnidade", u"CERMC", None))
        self.cbConcessionaria.setItemText(38, QCoreApplication.translate("frmCadastroUnidade", u"CERMISS\u00d5ES", None))
        self.cbConcessionaria.setItemText(39, QCoreApplication.translate("frmCadastroUnidade", u"CERMOFUL", None))
        self.cbConcessionaria.setItemText(40, QCoreApplication.translate("frmCadastroUnidade", u"CERNHE", None))
        self.cbConcessionaria.setItemText(41, QCoreApplication.translate("frmCadastroUnidade", u"CERON", None))
        self.cbConcessionaria.setItemText(42, QCoreApplication.translate("frmCadastroUnidade", u"CERPALO", None))
        self.cbConcessionaria.setItemText(43, QCoreApplication.translate("frmCadastroUnidade", u"CERPRO", None))
        self.cbConcessionaria.setItemText(44, QCoreApplication.translate("frmCadastroUnidade", u"CERR", None))
        self.cbConcessionaria.setItemText(45, QCoreApplication.translate("frmCadastroUnidade", u"CERRP", None))
        self.cbConcessionaria.setItemText(46, QCoreApplication.translate("frmCadastroUnidade", u"CERSAD\u00a0DISTRIBUIDORA", None))
        self.cbConcessionaria.setItemText(47, QCoreApplication.translate("frmCadastroUnidade", u"CERSUL", None))
        self.cbConcessionaria.setItemText(48, QCoreApplication.translate("frmCadastroUnidade", u"CERTAJA", None))
        self.cbConcessionaria.setItemText(49, QCoreApplication.translate("frmCadastroUnidade", u"CERTEL", None))
        self.cbConcessionaria.setItemText(50, QCoreApplication.translate("frmCadastroUnidade", u"CERTHIL", None))
        self.cbConcessionaria.setItemText(51, QCoreApplication.translate("frmCadastroUnidade", u"CERTREL", None))
        self.cbConcessionaria.setItemText(52, QCoreApplication.translate("frmCadastroUnidade", u"CERVAM", None))
        self.cbConcessionaria.setItemText(53, QCoreApplication.translate("frmCadastroUnidade", u"CETRIL", None))
        self.cbConcessionaria.setItemText(54, QCoreApplication.translate("frmCadastroUnidade", u"CFLO", None))
        self.cbConcessionaria.setItemText(55, QCoreApplication.translate("frmCadastroUnidade", u"CHESP", None))
        self.cbConcessionaria.setItemText(56, QCoreApplication.translate("frmCadastroUnidade", u"CNEE", None))
        self.cbConcessionaria.setItemText(57, QCoreApplication.translate("frmCadastroUnidade", u"COCEL", None))
        self.cbConcessionaria.setItemText(58, QCoreApplication.translate("frmCadastroUnidade", u"CODESAM", None))
        self.cbConcessionaria.setItemText(59, QCoreApplication.translate("frmCadastroUnidade", u"COELBA", None))
        self.cbConcessionaria.setItemText(60, QCoreApplication.translate("frmCadastroUnidade", u"COOPERA", None))
        self.cbConcessionaria.setItemText(61, QCoreApplication.translate("frmCadastroUnidade", u"COOPERALIAN\u00c7A", None))
        self.cbConcessionaria.setItemText(62, QCoreApplication.translate("frmCadastroUnidade", u"COOPERCOCAL", None))
        self.cbConcessionaria.setItemText(63, QCoreApplication.translate("frmCadastroUnidade", u"COOPERLUZ", None))
        self.cbConcessionaria.setItemText(64, QCoreApplication.translate("frmCadastroUnidade", u"COOPERMILA", None))
        self.cbConcessionaria.setItemText(65, QCoreApplication.translate("frmCadastroUnidade", u"COOPERNORTE", None))
        self.cbConcessionaria.setItemText(66, QCoreApplication.translate("frmCadastroUnidade", u"COOPERSUL", None))
        self.cbConcessionaria.setItemText(67, QCoreApplication.translate("frmCadastroUnidade", u"COOPERZEM", None))
        self.cbConcessionaria.setItemText(68, QCoreApplication.translate("frmCadastroUnidade", u"COORSEL", None))
        self.cbConcessionaria.setItemText(69, QCoreApplication.translate("frmCadastroUnidade", u"COPEL-DIS", None))
        self.cbConcessionaria.setItemText(70, QCoreApplication.translate("frmCadastroUnidade", u"COPREL", None))
        self.cbConcessionaria.setItemText(71, QCoreApplication.translate("frmCadastroUnidade", u"COSERN", None))
        self.cbConcessionaria.setItemText(72, QCoreApplication.translate("frmCadastroUnidade", u"CPFL\u00a0Jaguari", None))
        self.cbConcessionaria.setItemText(73, QCoreApplication.translate("frmCadastroUnidade", u"CPFL\u00a0Leste\u00a0Paulista", None))
        self.cbConcessionaria.setItemText(74, QCoreApplication.translate("frmCadastroUnidade", u"CPFL\u00a0Mococa", None))
        self.cbConcessionaria.setItemText(75, QCoreApplication.translate("frmCadastroUnidade", u"CPFL-\u00a0PIRATININGA", None))
        self.cbConcessionaria.setItemText(76, QCoreApplication.translate("frmCadastroUnidade", u"CPFL\u00a0Santa\u00a0Cruz", None))
        self.cbConcessionaria.setItemText(77, QCoreApplication.translate("frmCadastroUnidade", u"CPFL\u00a0Sul\u00a0Paulista", None))
        self.cbConcessionaria.setItemText(78, QCoreApplication.translate("frmCadastroUnidade", u"CPFL-PAULISTA", None))
        self.cbConcessionaria.setItemText(79, QCoreApplication.translate("frmCadastroUnidade", u"CRELUZ-D", None))
        self.cbConcessionaria.setItemText(80, QCoreApplication.translate("frmCadastroUnidade", u"CRERAL", None))
        self.cbConcessionaria.setItemText(81, QCoreApplication.translate("frmCadastroUnidade", u"DEMEI", None))
        self.cbConcessionaria.setItemText(82, QCoreApplication.translate("frmCadastroUnidade", u"DMED", None))
        self.cbConcessionaria.setItemText(83, QCoreApplication.translate("frmCadastroUnidade", u"EBO", None))
        self.cbConcessionaria.setItemText(84, QCoreApplication.translate("frmCadastroUnidade", u"EDEVP", None))
        self.cbConcessionaria.setItemText(85, QCoreApplication.translate("frmCadastroUnidade", u"EDP\u00a0ES", None))
        self.cbConcessionaria.setItemText(86, QCoreApplication.translate("frmCadastroUnidade", u"EDP\u00a0SP", None))
        self.cbConcessionaria.setItemText(87, QCoreApplication.translate("frmCadastroUnidade", u"EEB", None))
        self.cbConcessionaria.setItemText(88, QCoreApplication.translate("frmCadastroUnidade", u"EFLJC", None))
        self.cbConcessionaria.setItemText(89, QCoreApplication.translate("frmCadastroUnidade", u"EFLUL", None))
        self.cbConcessionaria.setItemText(90, QCoreApplication.translate("frmCadastroUnidade", u"ELEKTRO", None))
        self.cbConcessionaria.setItemText(91, QCoreApplication.translate("frmCadastroUnidade", u"ELETROACRE", None))
        self.cbConcessionaria.setItemText(92, QCoreApplication.translate("frmCadastroUnidade", u"ELETROCAR", None))
        self.cbConcessionaria.setItemText(93, QCoreApplication.translate("frmCadastroUnidade", u"ELETROPAULO", None))
        self.cbConcessionaria.setItemText(94, QCoreApplication.translate("frmCadastroUnidade", u"ELFSM", None))
        self.cbConcessionaria.setItemText(95, QCoreApplication.translate("frmCadastroUnidade", u"EMG", None))
        self.cbConcessionaria.setItemText(96, QCoreApplication.translate("frmCadastroUnidade", u"EMS", None))
        self.cbConcessionaria.setItemText(97, QCoreApplication.translate("frmCadastroUnidade", u"EMT", None))
        self.cbConcessionaria.setItemText(98, QCoreApplication.translate("frmCadastroUnidade", u"ENEL\u00a0CE", None))
        self.cbConcessionaria.setItemText(99, QCoreApplication.translate("frmCadastroUnidade", u"ENEL\u00a0RJ", None))
        self.cbConcessionaria.setItemText(100, QCoreApplication.translate("frmCadastroUnidade", u"ENF", None))
        self.cbConcessionaria.setItemText(101, QCoreApplication.translate("frmCadastroUnidade", u"EPB", None))
        self.cbConcessionaria.setItemText(102, QCoreApplication.translate("frmCadastroUnidade", u"Equatorial\u00a0AL", None))
        self.cbConcessionaria.setItemText(103, QCoreApplication.translate("frmCadastroUnidade", u"Equatorial\u00a0PA", None))
        self.cbConcessionaria.setItemText(104, QCoreApplication.translate("frmCadastroUnidade", u"Equatorial\u00a0PI", None))
        self.cbConcessionaria.setItemText(105, QCoreApplication.translate("frmCadastroUnidade", u"ESE", None))
        self.cbConcessionaria.setItemText(106, QCoreApplication.translate("frmCadastroUnidade", u"ESS", None))
        self.cbConcessionaria.setItemText(107, QCoreApplication.translate("frmCadastroUnidade", u"ETO", None))
        self.cbConcessionaria.setItemText(108, QCoreApplication.translate("frmCadastroUnidade", u"FORCEL", None))
        self.cbConcessionaria.setItemText(109, QCoreApplication.translate("frmCadastroUnidade", u"HIDROPAN", None))
        self.cbConcessionaria.setItemText(110, QCoreApplication.translate("frmCadastroUnidade", u"IENERGIA", None))
        self.cbConcessionaria.setItemText(111, QCoreApplication.translate("frmCadastroUnidade", u"LIGHT", None))
        self.cbConcessionaria.setItemText(112, QCoreApplication.translate("frmCadastroUnidade", u"MUXENERGIA", None))
        self.cbConcessionaria.setItemText(113, QCoreApplication.translate("frmCadastroUnidade", u"RGE", None))
        self.cbConcessionaria.setItemText(114, QCoreApplication.translate("frmCadastroUnidade", u"RGE\u00a0SUL", None))
        self.cbConcessionaria.setItemText(115, QCoreApplication.translate("frmCadastroUnidade", u"SULGIPE", None))
        self.cbConcessionaria.setItemText(116, QCoreApplication.translate("frmCadastroUnidade", u"UHENPAL", None))

        self.cbConcessionaria.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Concession\u00e1ria", None))
        self.label_7.setText(QCoreApplication.translate("frmCadastroUnidade", u"Tens\u00e3o Nominal:", None))
        self.label_3.setText(QCoreApplication.translate("frmCadastroUnidade", u"N\u00famero da UC:", None))
        self.cbTensaoNominal.setItemText(0, QCoreApplication.translate("frmCadastroUnidade", u"13800", None))
        self.cbTensaoNominal.setItemText(1, QCoreApplication.translate("frmCadastroUnidade", u"34500", None))

        self.cbTensaoNominal.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Tens\u00e3o Nominal", None))
        self.leNumeroUc.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"N\u00famero da UC", None))
        self.label_4.setText(QCoreApplication.translate("frmCadastroUnidade", u"N\u00famero do Medidor:", None))
        self.label_5.setText(QCoreApplication.translate("frmCadastroUnidade", u"C\u00f3digo do Cliente:", None))
        self.leNumeroMedidor.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"N\u00famero do Medidor", None))
        self.leCodigoCliente.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"C\u00f3digo do Cliente", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("frmCadastroUnidade", u"Dados do in\u00edcio do monitoramento", None))
        self.label_15.setText(QCoreApplication.translate("frmCadastroUnidade", u"In\u00edcio do Moni.:", None))
        self.label_8.setText(QCoreApplication.translate("frmCadastroUnidade", u"Tipo de Cliente:", None))
        self.deInicioMonitoramento.setDisplayFormat(QCoreApplication.translate("frmCadastroUnidade", u"MMM/yyyy", None))
        self.deInicioMonitoramento.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"In\u00edcio do Monitoramento", None))
        self.cbTipoCliente.setItemText(0, QCoreApplication.translate("frmCadastroUnidade", u"SAZONAL", None))
        self.cbTipoCliente.setItemText(1, QCoreApplication.translate("frmCadastroUnidade", u"INDUSTRIAL", None))

        self.cbTipoCliente.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Teipo de Cliente", None))
        self.label_9.setText(QCoreApplication.translate("frmCadastroUnidade", u"Modalidade:", None))
        self.label_10.setText(QCoreApplication.translate("frmCadastroUnidade", u"Grupo T.:", None))
        self.cbModalidade.setItemText(0, QCoreApplication.translate("frmCadastroUnidade", u"VERDE", None))
        self.cbModalidade.setItemText(1, QCoreApplication.translate("frmCadastroUnidade", u"AZUL", None))

        self.cbModalidade.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Modalidade", None))
        self.cbGrupoT.setItemText(0, QCoreApplication.translate("frmCadastroUnidade", u"A", None))
        self.cbGrupoT.setItemText(1, QCoreApplication.translate("frmCadastroUnidade", u"B", None))

        self.cbGrupoT.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Grupo Tarif\u00e1rio", None))
        self.label_11.setText(QCoreApplication.translate("frmCadastroUnidade", u"Subgrupo T.:", None))
        self.label_12.setText(QCoreApplication.translate("frmCadastroUnidade", u"Demanda F.P.:", None))
        self.cbSubgrupoT.setItemText(0, QCoreApplication.translate("frmCadastroUnidade", u"A3A", None))
        self.cbSubgrupoT.setItemText(1, QCoreApplication.translate("frmCadastroUnidade", u"A4", None))

        self.cbSubgrupoT.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Subgrupo Tarif\u00e1rio", None))
        self.leDemandaFp.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Demanda F.P.", None))
        self.label_13.setText(QCoreApplication.translate("frmCadastroUnidade", u"Demanda P.:", None))
        self.label_14.setText(QCoreApplication.translate("frmCadastroUnidade", u"ICMS:", None))
        self.leDemandaP.setText("")
        self.leDemandaP.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"Demanda P.", None))
        self.leIcms.setText("")
        self.leIcms.setProperty("name", QCoreApplication.translate("frmCadastroUnidade", u"ICMS", None))
#if QT_CONFIG(statustip)
        self.pbCancelar.setStatusTip(QCoreApplication.translate("frmCadastroUnidade", u"hehehe", None))
#endif // QT_CONFIG(statustip)
        self.pbCancelar.setText(QCoreApplication.translate("frmCadastroUnidade", u"Cancelar", None))
        self.pbSalvar.setText(QCoreApplication.translate("frmCadastroUnidade", u"Salvar", None))
    # retranslateUi

