# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_frm_grupos.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from dunidades.objetos import RQLineEdit


class Ui_frmGrupos(object):
    def setupUi(self, frmGrupos):
        if not frmGrupos.objectName():
            frmGrupos.setObjectName(u"frmGrupos")
        frmGrupos.resize(544, 313)
        self.verticalLayout = QVBoxLayout(frmGrupos)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(frmGrupos)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.leNome = RQLineEdit(frmGrupos)
        self.leNome.setObjectName(u"leNome")

        self.horizontalLayout.addWidget(self.leNome)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.pbLimpar = QPushButton(frmGrupos)
        self.pbLimpar.setObjectName(u"pbLimpar")
        self.pbLimpar.setAutoDefault(False)

        self.horizontalLayout_2.addWidget(self.pbLimpar)

        self.pbSalvar = QPushButton(frmGrupos)
        self.pbSalvar.setObjectName(u"pbSalvar")
        self.pbSalvar.setAutoDefault(False)

        self.horizontalLayout_2.addWidget(self.pbSalvar)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.groupBox_2 = QGroupBox(frmGrupos)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pbRecarregar = QPushButton(self.groupBox_2)
        self.pbRecarregar.setObjectName(u"pbRecarregar")
        self.pbRecarregar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbRecarregar)

        self.pbEditar = QPushButton(self.groupBox_2)
        self.pbEditar.setObjectName(u"pbEditar")
        self.pbEditar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbEditar)

        self.pbDeletar = QPushButton(self.groupBox_2)
        self.pbDeletar.setObjectName(u"pbDeletar")
        self.pbDeletar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbDeletar)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)

        self.lePesquisarTexto = QLineEdit(self.groupBox_2)
        self.lePesquisarTexto.setObjectName(u"lePesquisarTexto")
        self.lePesquisarTexto.setMinimumSize(QSize(140, 0))

        self.horizontalLayout_3.addWidget(self.lePesquisarTexto)

        self.pbPesquisar = QPushButton(self.groupBox_2)
        self.pbPesquisar.setObjectName(u"pbPesquisar")
        self.pbPesquisar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbPesquisar)


        self.verticalLayout_2.addLayout(self.horizontalLayout_3)

        self.twGrupos = QTableWidget(self.groupBox_2)
        if (self.twGrupos.columnCount() < 2):
            self.twGrupos.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.twGrupos.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        __qtablewidgetitem1.setTextAlignment(Qt.AlignLeading|Qt.AlignVCenter);
        self.twGrupos.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        self.twGrupos.setObjectName(u"twGrupos")
        self.twGrupos.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.twGrupos.setAutoScroll(False)
        self.twGrupos.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.twGrupos.setTabKeyNavigation(False)
        self.twGrupos.setSelectionMode(QAbstractItemView.SingleSelection)
        self.twGrupos.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.twGrupos.horizontalHeader().setHighlightSections(False)
        self.twGrupos.horizontalHeader().setStretchLastSection(False)
        self.twGrupos.verticalHeader().setVisible(False)
        self.twGrupos.verticalHeader().setDefaultSectionSize(23)

        self.verticalLayout_2.addWidget(self.twGrupos)


        self.verticalLayout.addWidget(self.groupBox_2)

        QWidget.setTabOrder(self.leNome, self.pbLimpar)
        QWidget.setTabOrder(self.pbLimpar, self.pbSalvar)
        QWidget.setTabOrder(self.pbSalvar, self.pbRecarregar)
        QWidget.setTabOrder(self.pbRecarregar, self.pbEditar)
        QWidget.setTabOrder(self.pbEditar, self.pbDeletar)
        QWidget.setTabOrder(self.pbDeletar, self.lePesquisarTexto)
        QWidget.setTabOrder(self.lePesquisarTexto, self.pbPesquisar)
        QWidget.setTabOrder(self.pbPesquisar, self.twGrupos)

        self.retranslateUi(frmGrupos)

        QMetaObject.connectSlotsByName(frmGrupos)
    # setupUi

    def retranslateUi(self, frmGrupos):
        frmGrupos.setWindowTitle(QCoreApplication.translate("frmGrupos", u"Grupos", None))
        self.label.setText(QCoreApplication.translate("frmGrupos", u"Nome:", None))
        self.leNome.setProperty("name", QCoreApplication.translate("frmGrupos", u"Nome", None))
        self.pbLimpar.setText(QCoreApplication.translate("frmGrupos", u"Limpar", None))
        self.pbSalvar.setText(QCoreApplication.translate("frmGrupos", u"Salvar", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("frmGrupos", u"Grupos", None))
        self.pbRecarregar.setText(QCoreApplication.translate("frmGrupos", u"Recarregar", None))
        self.pbEditar.setText(QCoreApplication.translate("frmGrupos", u"Editar", None))
        self.pbDeletar.setText(QCoreApplication.translate("frmGrupos", u"Deletar", None))
        self.lePesquisarTexto.setText("")
        self.pbPesquisar.setText(QCoreApplication.translate("frmGrupos", u"Pesquisar", None))
        ___qtablewidgetitem = self.twGrupos.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("frmGrupos", u"Id", None));
        ___qtablewidgetitem1 = self.twGrupos.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("frmGrupos", u"Nome", None));
    # retranslateUi

