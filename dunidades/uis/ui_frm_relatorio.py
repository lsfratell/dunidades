# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_frm_relatorio.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from dunidades.objetos import VQCheckBox


class Ui_frmRelatorio(object):
    def setupUi(self, frmRelatorio):
        if not frmRelatorio.objectName():
            frmRelatorio.setObjectName(u"frmRelatorio")
        frmRelatorio.resize(696, 469)
        self.gridLayout = QGridLayout(frmRelatorio)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gbUltrapassagem = QGroupBox(frmRelatorio)
        self.gbUltrapassagem.setObjectName(u"gbUltrapassagem")
        self.verticalLayout_2 = QVBoxLayout(self.gbUltrapassagem)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.ckUltr1 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr1.setObjectName(u"ckUltr1")
        self.ckUltr1.setChecked(True)
        self.ckUltr1.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr1.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr1)

        self.ckUltr2 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr2.setObjectName(u"ckUltr2")
        self.ckUltr2.setChecked(True)
        self.ckUltr2.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr2.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr2)

        self.ckUltr3 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr3.setObjectName(u"ckUltr3")
        self.ckUltr3.setChecked(True)
        self.ckUltr3.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr3.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr3)

        self.ckUltr4 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr4.setObjectName(u"ckUltr4")
        self.ckUltr4.setChecked(True)
        self.ckUltr4.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr4.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr4)

        self.ckUltr5 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr5.setObjectName(u"ckUltr5")
        self.ckUltr5.setChecked(True)
        self.ckUltr5.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr5.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr5)

        self.ckUltr6 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr6.setObjectName(u"ckUltr6")
        self.ckUltr6.setChecked(True)
        self.ckUltr6.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr6.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr6)

        self.ckUltr7 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr7.setObjectName(u"ckUltr7")
        self.ckUltr7.setChecked(True)
        self.ckUltr7.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr7.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr7)

        self.ckUltr8 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr8.setObjectName(u"ckUltr8")
        self.ckUltr8.setChecked(True)
        self.ckUltr8.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr8.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr8)

        self.ckUltr9 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr9.setObjectName(u"ckUltr9")
        self.ckUltr9.setChecked(True)
        self.ckUltr9.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr9.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr9)

        self.ckUltr10 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr10.setObjectName(u"ckUltr10")
        self.ckUltr10.setChecked(True)
        self.ckUltr10.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr10.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr10)

        self.ckUltr11 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr11.setObjectName(u"ckUltr11")
        self.ckUltr11.setChecked(True)
        self.ckUltr11.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr11.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr11)

        self.ckUltr12 = VQCheckBox(self.gbUltrapassagem)
        self.ckUltr12.setObjectName(u"ckUltr12")
        self.ckUltr12.setChecked(True)
        self.ckUltr12.setProperty("mes", QDate(0, 0, 0))
        self.ckUltr12.setProperty("valor", 0.000000000000000)

        self.verticalLayout_2.addWidget(self.ckUltr12)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_2.addItem(self.verticalSpacer_4)


        self.gridLayout.addWidget(self.gbUltrapassagem, 1, 1, 2, 1)

        self.gbCompensacoes = QGroupBox(frmRelatorio)
        self.gbCompensacoes.setObjectName(u"gbCompensacoes")
        self.verticalLayout = QVBoxLayout(self.gbCompensacoes)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.ckDev1 = VQCheckBox(self.gbCompensacoes)
        self.ckDev1.setObjectName(u"ckDev1")
        self.ckDev1.setChecked(True)
        self.ckDev1.setProperty("mes", QDate(0, 0, 0))
        self.ckDev1.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev1)

        self.ckDev2 = VQCheckBox(self.gbCompensacoes)
        self.ckDev2.setObjectName(u"ckDev2")
        self.ckDev2.setChecked(True)
        self.ckDev2.setProperty("mes", QDate(0, 0, 0))
        self.ckDev2.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev2)

        self.ckDev3 = VQCheckBox(self.gbCompensacoes)
        self.ckDev3.setObjectName(u"ckDev3")
        self.ckDev3.setChecked(True)
        self.ckDev3.setProperty("mes", QDate(0, 0, 0))
        self.ckDev3.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev3)

        self.ckDev4 = VQCheckBox(self.gbCompensacoes)
        self.ckDev4.setObjectName(u"ckDev4")
        self.ckDev4.setChecked(True)
        self.ckDev4.setProperty("mes", QDate(0, 0, 0))
        self.ckDev4.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev4)

        self.ckDev5 = VQCheckBox(self.gbCompensacoes)
        self.ckDev5.setObjectName(u"ckDev5")
        self.ckDev5.setChecked(True)
        self.ckDev5.setProperty("mes", QDate(0, 0, 0))
        self.ckDev5.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev5)

        self.ckDev6 = VQCheckBox(self.gbCompensacoes)
        self.ckDev6.setObjectName(u"ckDev6")
        self.ckDev6.setChecked(True)
        self.ckDev6.setProperty("mes", QDate(0, 0, 0))
        self.ckDev6.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev6)

        self.ckDev7 = VQCheckBox(self.gbCompensacoes)
        self.ckDev7.setObjectName(u"ckDev7")
        self.ckDev7.setChecked(True)
        self.ckDev7.setProperty("mes", QDate(0, 0, 0))
        self.ckDev7.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev7)

        self.ckDev8 = VQCheckBox(self.gbCompensacoes)
        self.ckDev8.setObjectName(u"ckDev8")
        self.ckDev8.setChecked(True)
        self.ckDev8.setProperty("mes", QDate(0, 0, 0))
        self.ckDev8.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev8)

        self.ckDev9 = VQCheckBox(self.gbCompensacoes)
        self.ckDev9.setObjectName(u"ckDev9")
        self.ckDev9.setChecked(True)
        self.ckDev9.setProperty("mes", QDate(0, 0, 0))
        self.ckDev9.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev9)

        self.ckDev10 = VQCheckBox(self.gbCompensacoes)
        self.ckDev10.setObjectName(u"ckDev10")
        self.ckDev10.setChecked(True)
        self.ckDev10.setProperty("mes", QDate(0, 0, 0))
        self.ckDev10.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev10)

        self.ckDev11 = VQCheckBox(self.gbCompensacoes)
        self.ckDev11.setObjectName(u"ckDev11")
        self.ckDev11.setChecked(True)
        self.ckDev11.setProperty("mes", QDate(0, 0, 0))
        self.ckDev11.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev11)

        self.ckDev12 = VQCheckBox(self.gbCompensacoes)
        self.ckDev12.setObjectName(u"ckDev12")
        self.ckDev12.setChecked(True)
        self.ckDev12.setProperty("mes", QDate(0, 0, 0))
        self.ckDev12.setProperty("valor", 0.000000000000000)

        self.verticalLayout.addWidget(self.ckDev12)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer_3)


        self.gridLayout.addWidget(self.gbCompensacoes, 1, 0, 2, 1)

        self.gbDemandaComplementar = QGroupBox(frmRelatorio)
        self.gbDemandaComplementar.setObjectName(u"gbDemandaComplementar")
        self.verticalLayout_3 = QVBoxLayout(self.gbDemandaComplementar)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.ckDem1 = VQCheckBox(self.gbDemandaComplementar)
        self.ckDem1.setObjectName(u"ckDem1")
        self.ckDem1.setChecked(True)
        self.ckDem1.setProperty("mes", QDate(0, 0, 0))
        self.ckDem1.setProperty("valor", 0.000000000000000)

        self.verticalLayout_3.addWidget(self.ckDem1)

        self.ckDem2 = VQCheckBox(self.gbDemandaComplementar)
        self.ckDem2.setObjectName(u"ckDem2")
        self.ckDem2.setChecked(True)
        self.ckDem2.setProperty("mes", QDate(0, 0, 0))
        self.ckDem2.setProperty("valor", 0.000000000000000)

        self.verticalLayout_3.addWidget(self.ckDem2)

        self.ckDem3 = VQCheckBox(self.gbDemandaComplementar)
        self.ckDem3.setObjectName(u"ckDem3")
        self.ckDem3.setChecked(True)
        self.ckDem3.setTristate(False)
        self.ckDem3.setProperty("mes", QDate(0, 0, 0))
        self.ckDem3.setProperty("valor", 0.000000000000000)

        self.verticalLayout_3.addWidget(self.ckDem3)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_3.addItem(self.verticalSpacer_2)


        self.gridLayout.addWidget(self.gbDemandaComplementar, 1, 2, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_3 = QLabel(frmRelatorio)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.label_3)

        self.deInicioMonitoramento = QDateEdit(frmRelatorio)
        self.deInicioMonitoramento.setObjectName(u"deInicioMonitoramento")
        self.deInicioMonitoramento.setReadOnly(True)

        self.horizontalLayout.addWidget(self.deInicioMonitoramento)

        self.label_4 = QLabel(frmRelatorio)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.label_4)

        self.cbTipoRelatorio = QComboBox(frmRelatorio)
        self.cbTipoRelatorio.addItem("")
        self.cbTipoRelatorio.setObjectName(u"cbTipoRelatorio")

        self.horizontalLayout.addWidget(self.cbTipoRelatorio)

        self.label = QLabel(frmRelatorio)
        self.label.setObjectName(u"label")
        self.label.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.label)

        self.cbDataInicio = QComboBox(frmRelatorio)
        self.cbDataInicio.setObjectName(u"cbDataInicio")

        self.horizontalLayout.addWidget(self.cbDataInicio)

        self.label_2 = QLabel(frmRelatorio)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.horizontalLayout.addWidget(self.label_2)

        self.cbDataFinal = QComboBox(frmRelatorio)
        self.cbDataFinal.setObjectName(u"cbDataFinal")

        self.horizontalLayout.addWidget(self.cbDataFinal)

        self.pbAnalisar = QPushButton(frmRelatorio)
        self.pbAnalisar.setObjectName(u"pbAnalisar")
        self.pbAnalisar.setAutoDefault(False)

        self.horizontalLayout.addWidget(self.pbAnalisar)

        self.pbGerar = QPushButton(frmRelatorio)
        self.pbGerar.setObjectName(u"pbGerar")
        self.pbGerar.setAutoDefault(False)

        self.horizontalLayout.addWidget(self.pbGerar)


        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 3)

        self.gbDemandaComplementarFaturada = QGroupBox(frmRelatorio)
        self.gbDemandaComplementarFaturada.setObjectName(u"gbDemandaComplementarFaturada")
        self.verticalLayout_4 = QVBoxLayout(self.gbDemandaComplementarFaturada)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.ckDemFat1 = VQCheckBox(self.gbDemandaComplementarFaturada)
        self.ckDemFat1.setObjectName(u"ckDemFat1")
        self.ckDemFat1.setChecked(True)
        self.ckDemFat1.setProperty("mes", QDate(0, 0, 0))
        self.ckDemFat1.setProperty("valor", 0.000000000000000)

        self.verticalLayout_4.addWidget(self.ckDemFat1)

        self.ckDemFat2 = VQCheckBox(self.gbDemandaComplementarFaturada)
        self.ckDemFat2.setObjectName(u"ckDemFat2")
        self.ckDemFat2.setChecked(True)
        self.ckDemFat2.setProperty("mes", QDate(0, 0, 0))
        self.ckDemFat2.setProperty("valor", 0.000000000000000)

        self.verticalLayout_4.addWidget(self.ckDemFat2)

        self.ckDemFat3 = VQCheckBox(self.gbDemandaComplementarFaturada)
        self.ckDemFat3.setObjectName(u"ckDemFat3")
        self.ckDemFat3.setChecked(True)
        self.ckDemFat3.setProperty("mes", QDate(0, 0, 0))
        self.ckDemFat3.setProperty("valor", 0.000000000000000)

        self.verticalLayout_4.addWidget(self.ckDemFat3)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer)


        self.gridLayout.addWidget(self.gbDemandaComplementarFaturada, 2, 2, 1, 1)

        self.teObservacoes = QPlainTextEdit(frmRelatorio)
        self.teObservacoes.setObjectName(u"teObservacoes")

        self.gridLayout.addWidget(self.teObservacoes, 3, 0, 1, 3)


        self.retranslateUi(frmRelatorio)

        QMetaObject.connectSlotsByName(frmRelatorio)
    # setupUi

    def retranslateUi(self, frmRelatorio):
        frmRelatorio.setWindowTitle(QCoreApplication.translate("frmRelatorio", u"Relat\u00f3rios", None))
        self.gbUltrapassagem.setTitle(QCoreApplication.translate("frmRelatorio", u"Ultrapassagem de Demanda", None))
        self.ckUltr1.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr2.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr3.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr4.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr5.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr6.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr7.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr8.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr9.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr10.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr11.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckUltr12.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.gbCompensacoes.setTitle(QCoreApplication.translate("frmRelatorio", u"Devolu\u00e7\u00f5es/Compensa\u00e7\u00f5es", None))
        self.ckDev1.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev2.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev3.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev4.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev5.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev6.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev7.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev8.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev9.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev10.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev11.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDev12.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.gbDemandaComplementar.setTitle(QCoreApplication.translate("frmRelatorio", u"Demanda Complementar", None))
        self.ckDem1.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDem2.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDem3.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.label_3.setText(QCoreApplication.translate("frmRelatorio", u"In\u00edcio M.:", None))
        self.deInicioMonitoramento.setDisplayFormat(QCoreApplication.translate("frmRelatorio", u"MMM/yyyy", None))
        self.label_4.setText(QCoreApplication.translate("frmRelatorio", u"Relat\u00f3rio:", None))
        self.cbTipoRelatorio.setItemText(0, QCoreApplication.translate("frmRelatorio", u"Verde Sazonal", None))

        self.label.setText(QCoreApplication.translate("frmRelatorio", u"D. In\u00edcio:", None))
        self.label_2.setText(QCoreApplication.translate("frmRelatorio", u"D. Final:", None))
        self.pbAnalisar.setText(QCoreApplication.translate("frmRelatorio", u"Analisar", None))
        self.pbGerar.setText(QCoreApplication.translate("frmRelatorio", u"Gerar", None))
        self.gbDemandaComplementarFaturada.setTitle(QCoreApplication.translate("frmRelatorio", u"Demanda Complementar Faturada", None))
        self.ckDemFat1.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDemFat2.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.ckDemFat3.setText(QCoreApplication.translate("frmRelatorio", u"CheckBox", None))
        self.teObservacoes.setPlainText(QCoreApplication.translate("frmRelatorio", u"Obs:", None))
    # retranslateUi

