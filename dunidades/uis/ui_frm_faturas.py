# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_frm_faturas.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(663, 352)
        self.verticalLayout = QVBoxLayout(Dialog)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pbCadastro = QPushButton(Dialog)
        self.pbCadastro.setObjectName(u"pbCadastro")
        self.pbCadastro.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbCadastro)

        self.pbCarregar = QPushButton(Dialog)
        self.pbCarregar.setObjectName(u"pbCarregar")
        self.pbCarregar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbCarregar)

        self.pbEditar = QPushButton(Dialog)
        self.pbEditar.setObjectName(u"pbEditar")
        self.pbEditar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbEditar)

        self.pbDeletar = QPushButton(Dialog)
        self.pbDeletar.setObjectName(u"pbDeletar")
        self.pbDeletar.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbDeletar)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.twFaturas = QTableWidget(Dialog)
        if (self.twFaturas.columnCount() < 6):
            self.twFaturas.setColumnCount(6)
        __qtablewidgetitem = QTableWidgetItem()
        self.twFaturas.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.twFaturas.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        self.twFaturas.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        self.twFaturas.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.twFaturas.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.twFaturas.setHorizontalHeaderItem(5, __qtablewidgetitem5)
        self.twFaturas.setObjectName(u"twFaturas")
        self.twFaturas.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.twFaturas.setAutoScroll(False)
        self.twFaturas.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.twFaturas.setTabKeyNavigation(False)
        self.twFaturas.setSelectionMode(QAbstractItemView.SingleSelection)
        self.twFaturas.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.twFaturas.horizontalHeader().setHighlightSections(False)
        self.twFaturas.horizontalHeader().setStretchLastSection(False)
        self.twFaturas.verticalHeader().setVisible(False)
        self.twFaturas.verticalHeader().setDefaultSectionSize(23)

        self.verticalLayout.addWidget(self.twFaturas)

        QWidget.setTabOrder(self.pbCadastro, self.pbCarregar)
        QWidget.setTabOrder(self.pbCarregar, self.pbEditar)
        QWidget.setTabOrder(self.pbEditar, self.pbDeletar)
        QWidget.setTabOrder(self.pbDeletar, self.twFaturas)

        self.retranslateUi(Dialog)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Faturas", None))
        self.pbCadastro.setText(QCoreApplication.translate("Dialog", u"Cadastro", None))
        self.pbCarregar.setText(QCoreApplication.translate("Dialog", u"Recarregar", None))
        self.pbEditar.setText(QCoreApplication.translate("Dialog", u"Editar", None))
        self.pbDeletar.setText(QCoreApplication.translate("Dialog", u"Deletar", None))
        ___qtablewidgetitem = self.twFaturas.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("Dialog", u"Id", None));
        ___qtablewidgetitem1 = self.twFaturas.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("Dialog", u"M\u00eas Ref.", None));
        ___qtablewidgetitem2 = self.twFaturas.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("Dialog", u"Vencimento", None));
        ___qtablewidgetitem3 = self.twFaturas.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("Dialog", u"Demanda F.P.", None));
        ___qtablewidgetitem4 = self.twFaturas.horizontalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("Dialog", u"Demanda P.", None));
        ___qtablewidgetitem5 = self.twFaturas.horizontalHeaderItem(5)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("Dialog", u"Valor", None));
    # retranslateUi

