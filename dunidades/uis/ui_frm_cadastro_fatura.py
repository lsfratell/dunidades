# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_frm_cadastro_fatura.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *

from dunidades.objetos import RQLineEdit
from dunidades.objetos import RQComboBox


class Ui_frmCadastroFatura(object):
    def setupUi(self, frmCadastroFatura):
        if not frmCadastroFatura.objectName():
            frmCadastroFatura.setObjectName(u"frmCadastroFatura")
        frmCadastroFatura.resize(636, 581)
        self.verticalLayout = QVBoxLayout(frmCadastroFatura)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.gridLayout = QGridLayout()
        self.gridLayout.setObjectName(u"gridLayout")
        self.label_13 = QLabel(frmCadastroFatura)
        self.label_13.setObjectName(u"label_13")

        self.gridLayout.addWidget(self.label_13, 0, 0, 1, 1)

        self.label_14 = QLabel(frmCadastroFatura)
        self.label_14.setObjectName(u"label_14")

        self.gridLayout.addWidget(self.label_14, 0, 2, 1, 1)

        self.cbModalidade = RQComboBox(frmCadastroFatura)
        self.cbModalidade.addItem("")
        self.cbModalidade.addItem("")
        self.cbModalidade.setObjectName(u"cbModalidade")

        self.gridLayout.addWidget(self.cbModalidade, 1, 0, 1, 2)

        self.cbTipoCliente = RQComboBox(frmCadastroFatura)
        self.cbTipoCliente.addItem("")
        self.cbTipoCliente.addItem("")
        self.cbTipoCliente.setObjectName(u"cbTipoCliente")

        self.gridLayout.addWidget(self.cbTipoCliente, 1, 2, 1, 2)

        self.label = QLabel(frmCadastroFatura)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 2, 0, 1, 1)

        self.label_2 = QLabel(frmCadastroFatura)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 2, 1, 1, 1)

        self.label_3 = QLabel(frmCadastroFatura)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 2, 2, 1, 1)

        self.label_4 = QLabel(frmCadastroFatura)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout.addWidget(self.label_4, 2, 3, 1, 1)

        self.deLeituraAnterior = QDateEdit(frmCadastroFatura)
        self.deLeituraAnterior.setObjectName(u"deLeituraAnterior")
        sizePolicy = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.deLeituraAnterior.sizePolicy().hasHeightForWidth())
        self.deLeituraAnterior.setSizePolicy(sizePolicy)
        self.deLeituraAnterior.setCalendarPopup(True)

        self.gridLayout.addWidget(self.deLeituraAnterior, 3, 0, 1, 1)

        self.deLeituraAtual = QDateEdit(frmCadastroFatura)
        self.deLeituraAtual.setObjectName(u"deLeituraAtual")
        sizePolicy.setHeightForWidth(self.deLeituraAtual.sizePolicy().hasHeightForWidth())
        self.deLeituraAtual.setSizePolicy(sizePolicy)
        self.deLeituraAtual.setCalendarPopup(True)

        self.gridLayout.addWidget(self.deLeituraAtual, 3, 1, 1, 1)

        self.deProximaLeitura = QDateEdit(frmCadastroFatura)
        self.deProximaLeitura.setObjectName(u"deProximaLeitura")
        sizePolicy.setHeightForWidth(self.deProximaLeitura.sizePolicy().hasHeightForWidth())
        self.deProximaLeitura.setSizePolicy(sizePolicy)
        self.deProximaLeitura.setCalendarPopup(True)

        self.gridLayout.addWidget(self.deProximaLeitura, 3, 2, 1, 1)

        self.leDiasFaturado = RQLineEdit(frmCadastroFatura)
        self.leDiasFaturado.setObjectName(u"leDiasFaturado")

        self.gridLayout.addWidget(self.leDiasFaturado, 3, 3, 1, 1)

        self.label_6 = QLabel(frmCadastroFatura)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout.addWidget(self.label_6, 4, 0, 1, 1)

        self.label_8 = QLabel(frmCadastroFatura)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout.addWidget(self.label_8, 4, 1, 1, 1)

        self.label_7 = QLabel(frmCadastroFatura)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout.addWidget(self.label_7, 4, 2, 1, 1)

        self.label_5 = QLabel(frmCadastroFatura)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout.addWidget(self.label_5, 4, 3, 1, 1)

        self.deMesReferencia = QDateEdit(frmCadastroFatura)
        self.deMesReferencia.setObjectName(u"deMesReferencia")
        sizePolicy.setHeightForWidth(self.deMesReferencia.sizePolicy().hasHeightForWidth())
        self.deMesReferencia.setSizePolicy(sizePolicy)
        self.deMesReferencia.setCalendarPopup(True)

        self.gridLayout.addWidget(self.deMesReferencia, 5, 0, 1, 1)

        self.deVencimento = QDateEdit(frmCadastroFatura)
        self.deVencimento.setObjectName(u"deVencimento")
        sizePolicy.setHeightForWidth(self.deVencimento.sizePolicy().hasHeightForWidth())
        self.deVencimento.setSizePolicy(sizePolicy)
        self.deVencimento.setCalendarPopup(True)

        self.gridLayout.addWidget(self.deVencimento, 5, 1, 1, 1)

        self.leValor = RQLineEdit(frmCadastroFatura)
        self.leValor.setObjectName(u"leValor")

        self.gridLayout.addWidget(self.leValor, 5, 2, 1, 1)

        self.leMedidor = RQLineEdit(frmCadastroFatura)
        self.leMedidor.setObjectName(u"leMedidor")

        self.gridLayout.addWidget(self.leMedidor, 5, 3, 1, 1)

        self.label_11 = QLabel(frmCadastroFatura)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout.addWidget(self.label_11, 6, 0, 1, 1)

        self.label_10 = QLabel(frmCadastroFatura)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout.addWidget(self.label_10, 6, 1, 1, 1)

        self.label_12 = QLabel(frmCadastroFatura)
        self.label_12.setObjectName(u"label_12")

        self.gridLayout.addWidget(self.label_12, 6, 2, 1, 1)

        self.label_9 = QLabel(frmCadastroFatura)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout.addWidget(self.label_9, 6, 3, 1, 1)

        self.deApresentacao = QDateEdit(frmCadastroFatura)
        self.deApresentacao.setObjectName(u"deApresentacao")
        sizePolicy.setHeightForWidth(self.deApresentacao.sizePolicy().hasHeightForWidth())
        self.deApresentacao.setSizePolicy(sizePolicy)
        self.deApresentacao.setCalendarPopup(True)

        self.gridLayout.addWidget(self.deApresentacao, 7, 0, 1, 1)

        self.leDemandaContratadaFp = RQLineEdit(frmCadastroFatura)
        self.leDemandaContratadaFp.setObjectName(u"leDemandaContratadaFp")

        self.gridLayout.addWidget(self.leDemandaContratadaFp, 7, 1, 1, 1)

        self.leDemandaContratadaP = RQLineEdit(frmCadastroFatura)
        self.leDemandaContratadaP.setObjectName(u"leDemandaContratadaP")

        self.gridLayout.addWidget(self.leDemandaContratadaP, 7, 2, 1, 1)

        self.lePerdaTransformador = RQLineEdit(frmCadastroFatura)
        self.lePerdaTransformador.setObjectName(u"lePerdaTransformador")

        self.gridLayout.addWidget(self.lePerdaTransformador, 7, 3, 1, 1)


        self.verticalLayout.addLayout(self.gridLayout)

        self.tabWidget = QTabWidget(frmCadastroFatura)
        self.tabWidget.setObjectName(u"tabWidget")
        self.tab = QWidget()
        self.tab.setObjectName(u"tab")
        self.gridLayout_16 = QGridLayout(self.tab)
        self.gridLayout_16.setObjectName(u"gridLayout_16")
        self.groupBox_6 = QGroupBox(self.tab)
        self.groupBox_6.setObjectName(u"groupBox_6")
        self.gridLayout_15 = QGridLayout(self.groupBox_6)
        self.gridLayout_15.setObjectName(u"gridLayout_15")
        self.label_64 = QLabel(self.groupBox_6)
        self.label_64.setObjectName(u"label_64")

        self.gridLayout_15.addWidget(self.label_64, 0, 0, 1, 1)

        self.label_65 = QLabel(self.groupBox_6)
        self.label_65.setObjectName(u"label_65")

        self.gridLayout_15.addWidget(self.label_65, 0, 1, 1, 1)

        self.label_66 = QLabel(self.groupBox_6)
        self.label_66.setObjectName(u"label_66")

        self.gridLayout_15.addWidget(self.label_66, 0, 2, 1, 1)

        self.leProdutosDemandaRegistradaP = RQLineEdit(self.groupBox_6)
        self.leProdutosDemandaRegistradaP.setObjectName(u"leProdutosDemandaRegistradaP")

        self.gridLayout_15.addWidget(self.leProdutosDemandaRegistradaP, 1, 0, 1, 1)

        self.leProdutosDemandaRegistradaFp = RQLineEdit(self.groupBox_6)
        self.leProdutosDemandaRegistradaFp.setObjectName(u"leProdutosDemandaRegistradaFp")

        self.gridLayout_15.addWidget(self.leProdutosDemandaRegistradaFp, 1, 1, 1, 1)

        self.leProdutosDemandaRegistradaHr = RQLineEdit(self.groupBox_6)
        self.leProdutosDemandaRegistradaHr.setObjectName(u"leProdutosDemandaRegistradaHr")

        self.gridLayout_15.addWidget(self.leProdutosDemandaRegistradaHr, 1, 2, 1, 1)


        self.gridLayout_16.addWidget(self.groupBox_6, 0, 0, 1, 1)

        self.groupBox_3 = QGroupBox(self.tab)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.gridLayout_2 = QGridLayout(self.groupBox_3)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label_18 = QLabel(self.groupBox_3)
        self.label_18.setObjectName(u"label_18")

        self.gridLayout_2.addWidget(self.label_18, 0, 0, 1, 1)

        self.label_19 = QLabel(self.groupBox_3)
        self.label_19.setObjectName(u"label_19")

        self.gridLayout_2.addWidget(self.label_19, 0, 1, 1, 1)

        self.label_27 = QLabel(self.groupBox_3)
        self.label_27.setObjectName(u"label_27")

        self.gridLayout_2.addWidget(self.label_27, 0, 2, 1, 1)

        self.leProdutosDemandaFaturadaP = RQLineEdit(self.groupBox_3)
        self.leProdutosDemandaFaturadaP.setObjectName(u"leProdutosDemandaFaturadaP")

        self.gridLayout_2.addWidget(self.leProdutosDemandaFaturadaP, 1, 0, 1, 1)

        self.leProdutosDemandaFaturadaFp = RQLineEdit(self.groupBox_3)
        self.leProdutosDemandaFaturadaFp.setObjectName(u"leProdutosDemandaFaturadaFp")

        self.gridLayout_2.addWidget(self.leProdutosDemandaFaturadaFp, 1, 1, 1, 1)

        self.leProdutosDemandaFaturadaHr = RQLineEdit(self.groupBox_3)
        self.leProdutosDemandaFaturadaHr.setObjectName(u"leProdutosDemandaFaturadaHr")

        self.gridLayout_2.addWidget(self.leProdutosDemandaFaturadaHr, 1, 2, 1, 1)


        self.gridLayout_16.addWidget(self.groupBox_3, 0, 1, 1, 1)

        self.groupBox_5 = QGroupBox(self.tab)
        self.groupBox_5.setObjectName(u"groupBox_5")
        self.gridLayout_5 = QGridLayout(self.groupBox_5)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.label_24 = QLabel(self.groupBox_5)
        self.label_24.setObjectName(u"label_24")

        self.gridLayout_5.addWidget(self.label_24, 0, 0, 1, 1)

        self.label_25 = QLabel(self.groupBox_5)
        self.label_25.setObjectName(u"label_25")

        self.gridLayout_5.addWidget(self.label_25, 0, 1, 1, 1)

        self.label_26 = QLabel(self.groupBox_5)
        self.label_26.setObjectName(u"label_26")

        self.gridLayout_5.addWidget(self.label_26, 0, 2, 1, 1)

        self.leProdutosConsumoP = RQLineEdit(self.groupBox_5)
        self.leProdutosConsumoP.setObjectName(u"leProdutosConsumoP")

        self.gridLayout_5.addWidget(self.leProdutosConsumoP, 1, 0, 1, 1)

        self.leProdutosConsumoFp = RQLineEdit(self.groupBox_5)
        self.leProdutosConsumoFp.setObjectName(u"leProdutosConsumoFp")

        self.gridLayout_5.addWidget(self.leProdutosConsumoFp, 1, 1, 1, 1)

        self.leProdutosConsumoHr = RQLineEdit(self.groupBox_5)
        self.leProdutosConsumoHr.setObjectName(u"leProdutosConsumoHr")

        self.gridLayout_5.addWidget(self.leProdutosConsumoHr, 1, 2, 1, 1)


        self.gridLayout_16.addWidget(self.groupBox_5, 1, 0, 1, 1)

        self.groupBox_15 = QGroupBox(self.tab)
        self.groupBox_15.setObjectName(u"groupBox_15")
        self.gridLayout_3 = QGridLayout(self.groupBox_15)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.label_73 = QLabel(self.groupBox_15)
        self.label_73.setObjectName(u"label_73")

        self.gridLayout_3.addWidget(self.label_73, 0, 0, 1, 1)

        self.label_74 = QLabel(self.groupBox_15)
        self.label_74.setObjectName(u"label_74")

        self.gridLayout_3.addWidget(self.label_74, 0, 1, 1, 1)

        self.label_79 = QLabel(self.groupBox_15)
        self.label_79.setObjectName(u"label_79")

        self.gridLayout_3.addWidget(self.label_79, 0, 2, 1, 1)

        self.leProdutosDmcrP = RQLineEdit(self.groupBox_15)
        self.leProdutosDmcrP.setObjectName(u"leProdutosDmcrP")

        self.gridLayout_3.addWidget(self.leProdutosDmcrP, 1, 0, 1, 1)

        self.leProdutosDmcrFp = RQLineEdit(self.groupBox_15)
        self.leProdutosDmcrFp.setObjectName(u"leProdutosDmcrFp")

        self.gridLayout_3.addWidget(self.leProdutosDmcrFp, 1, 1, 1, 1)

        self.leProdutosDmcrHr = RQLineEdit(self.groupBox_15)
        self.leProdutosDmcrHr.setObjectName(u"leProdutosDmcrHr")

        self.gridLayout_3.addWidget(self.leProdutosDmcrHr, 1, 2, 1, 1)


        self.gridLayout_16.addWidget(self.groupBox_15, 1, 1, 1, 1)

        self.groupBox_4 = QGroupBox(self.tab)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.gridLayout_4 = QGridLayout(self.groupBox_4)
        self.gridLayout_4.setObjectName(u"gridLayout_4")
        self.label_21 = QLabel(self.groupBox_4)
        self.label_21.setObjectName(u"label_21")

        self.gridLayout_4.addWidget(self.label_21, 0, 0, 1, 1)

        self.label_22 = QLabel(self.groupBox_4)
        self.label_22.setObjectName(u"label_22")

        self.gridLayout_4.addWidget(self.label_22, 0, 1, 1, 1)

        self.label_23 = QLabel(self.groupBox_4)
        self.label_23.setObjectName(u"label_23")

        self.gridLayout_4.addWidget(self.label_23, 0, 2, 1, 1)

        self.leProdutosUferP = RQLineEdit(self.groupBox_4)
        self.leProdutosUferP.setObjectName(u"leProdutosUferP")

        self.gridLayout_4.addWidget(self.leProdutosUferP, 1, 0, 1, 1)

        self.leProdutosUferFp = RQLineEdit(self.groupBox_4)
        self.leProdutosUferFp.setObjectName(u"leProdutosUferFp")

        self.gridLayout_4.addWidget(self.leProdutosUferFp, 1, 1, 1, 1)

        self.leProdutosUferHr = RQLineEdit(self.groupBox_4)
        self.leProdutosUferHr.setObjectName(u"leProdutosUferHr")

        self.gridLayout_4.addWidget(self.leProdutosUferHr, 1, 2, 1, 1)


        self.gridLayout_16.addWidget(self.groupBox_4, 2, 0, 1, 1)

        self.horizontalSpacer_3 = QSpacerItem(291, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_16.addItem(self.horizontalSpacer_3, 2, 1, 1, 1)

        self.verticalSpacer_3 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_16.addItem(self.verticalSpacer_3, 3, 0, 1, 1)

        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QWidget()
        self.tab_2.setObjectName(u"tab_2")
        self.gridLayout_11 = QGridLayout(self.tab_2)
        self.gridLayout_11.setObjectName(u"gridLayout_11")
        self.groupBox_7 = QGroupBox(self.tab_2)
        self.groupBox_7.setObjectName(u"groupBox_7")
        self.gridLayout_7 = QGridLayout(self.groupBox_7)
        self.gridLayout_7.setObjectName(u"gridLayout_7")
        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.label_30 = QLabel(self.groupBox_7)
        self.label_30.setObjectName(u"label_30")

        self.verticalLayout_2.addWidget(self.label_30)

        self.leTarifaPDemanda = RQLineEdit(self.groupBox_7)
        self.leTarifaPDemanda.setObjectName(u"leTarifaPDemanda")

        self.verticalLayout_2.addWidget(self.leTarifaPDemanda)


        self.gridLayout_7.addLayout(self.verticalLayout_2, 0, 0, 1, 1)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.label_31 = QLabel(self.groupBox_7)
        self.label_31.setObjectName(u"label_31")

        self.verticalLayout_3.addWidget(self.label_31)

        self.leTarifaPDemandaUltrapassagem = RQLineEdit(self.groupBox_7)
        self.leTarifaPDemandaUltrapassagem.setObjectName(u"leTarifaPDemandaUltrapassagem")

        self.verticalLayout_3.addWidget(self.leTarifaPDemandaUltrapassagem)


        self.gridLayout_7.addLayout(self.verticalLayout_3, 0, 1, 1, 1)

        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.label_37 = QLabel(self.groupBox_7)
        self.label_37.setObjectName(u"label_37")

        self.verticalLayout_4.addWidget(self.label_37)

        self.leTarifaPDmcr = RQLineEdit(self.groupBox_7)
        self.leTarifaPDmcr.setObjectName(u"leTarifaPDmcr")

        self.verticalLayout_4.addWidget(self.leTarifaPDmcr)


        self.gridLayout_7.addLayout(self.verticalLayout_4, 0, 2, 1, 1)

        self.verticalLayout_5 = QVBoxLayout()
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.label_34 = QLabel(self.groupBox_7)
        self.label_34.setObjectName(u"label_34")

        self.verticalLayout_5.addWidget(self.label_34)

        self.leTarifaPConsumoTe = RQLineEdit(self.groupBox_7)
        self.leTarifaPConsumoTe.setObjectName(u"leTarifaPConsumoTe")

        self.verticalLayout_5.addWidget(self.leTarifaPConsumoTe)


        self.gridLayout_7.addLayout(self.verticalLayout_5, 1, 0, 1, 1)

        self.verticalLayout_6 = QVBoxLayout()
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.label_35 = QLabel(self.groupBox_7)
        self.label_35.setObjectName(u"label_35")

        self.verticalLayout_6.addWidget(self.label_35)

        self.leTarifaPConsumoTusd = RQLineEdit(self.groupBox_7)
        self.leTarifaPConsumoTusd.setObjectName(u"leTarifaPConsumoTusd")

        self.verticalLayout_6.addWidget(self.leTarifaPConsumoTusd)


        self.gridLayout_7.addLayout(self.verticalLayout_6, 1, 1, 1, 1)

        self.verticalLayout_7 = QVBoxLayout()
        self.verticalLayout_7.setObjectName(u"verticalLayout_7")
        self.label_33 = QLabel(self.groupBox_7)
        self.label_33.setObjectName(u"label_33")

        self.verticalLayout_7.addWidget(self.label_33)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.leTarifaPConsumo = RQLineEdit(self.groupBox_7)
        self.leTarifaPConsumo.setObjectName(u"leTarifaPConsumo")

        self.horizontalLayout_2.addWidget(self.leTarifaPConsumo)

        self.pbSomarConsumoP = QPushButton(self.groupBox_7)
        self.pbSomarConsumoP.setObjectName(u"pbSomarConsumoP")
        self.pbSomarConsumoP.setMaximumSize(QSize(19, 20))
        self.pbSomarConsumoP.setAutoDefault(False)

        self.horizontalLayout_2.addWidget(self.pbSomarConsumoP)


        self.verticalLayout_7.addLayout(self.horizontalLayout_2)


        self.gridLayout_7.addLayout(self.verticalLayout_7, 1, 2, 1, 1)

        self.verticalLayout_8 = QVBoxLayout()
        self.verticalLayout_8.setObjectName(u"verticalLayout_8")
        self.label_36 = QLabel(self.groupBox_7)
        self.label_36.setObjectName(u"label_36")

        self.verticalLayout_8.addWidget(self.label_36)

        self.leTarifaPUfer = RQLineEdit(self.groupBox_7)
        self.leTarifaPUfer.setObjectName(u"leTarifaPUfer")

        self.verticalLayout_8.addWidget(self.leTarifaPUfer)


        self.gridLayout_7.addLayout(self.verticalLayout_8, 2, 0, 1, 1)


        self.gridLayout_11.addWidget(self.groupBox_7, 0, 0, 1, 1)

        self.groupBox_8 = QGroupBox(self.tab_2)
        self.groupBox_8.setObjectName(u"groupBox_8")
        self.gridLayout_8 = QGridLayout(self.groupBox_8)
        self.gridLayout_8.setObjectName(u"gridLayout_8")
        self.verticalLayout_13 = QVBoxLayout()
        self.verticalLayout_13.setObjectName(u"verticalLayout_13")
        self.label_32 = QLabel(self.groupBox_8)
        self.label_32.setObjectName(u"label_32")

        self.verticalLayout_13.addWidget(self.label_32)

        self.leTarifaFpDemanda = RQLineEdit(self.groupBox_8)
        self.leTarifaFpDemanda.setObjectName(u"leTarifaFpDemanda")

        self.verticalLayout_13.addWidget(self.leTarifaFpDemanda)


        self.gridLayout_8.addLayout(self.verticalLayout_13, 0, 0, 1, 1)

        self.verticalLayout_14 = QVBoxLayout()
        self.verticalLayout_14.setObjectName(u"verticalLayout_14")
        self.label_38 = QLabel(self.groupBox_8)
        self.label_38.setObjectName(u"label_38")

        self.verticalLayout_14.addWidget(self.label_38)

        self.leTarifaFpDemandaUltrapassagem = RQLineEdit(self.groupBox_8)
        self.leTarifaFpDemandaUltrapassagem.setObjectName(u"leTarifaFpDemandaUltrapassagem")

        self.verticalLayout_14.addWidget(self.leTarifaFpDemandaUltrapassagem)


        self.gridLayout_8.addLayout(self.verticalLayout_14, 0, 1, 1, 1)

        self.verticalLayout_15 = QVBoxLayout()
        self.verticalLayout_15.setObjectName(u"verticalLayout_15")
        self.label_39 = QLabel(self.groupBox_8)
        self.label_39.setObjectName(u"label_39")

        self.verticalLayout_15.addWidget(self.label_39)

        self.leTarifaFpDmcr = RQLineEdit(self.groupBox_8)
        self.leTarifaFpDmcr.setObjectName(u"leTarifaFpDmcr")

        self.verticalLayout_15.addWidget(self.leTarifaFpDmcr)


        self.gridLayout_8.addLayout(self.verticalLayout_15, 0, 2, 1, 1)

        self.verticalLayout_16 = QVBoxLayout()
        self.verticalLayout_16.setObjectName(u"verticalLayout_16")
        self.label_40 = QLabel(self.groupBox_8)
        self.label_40.setObjectName(u"label_40")

        self.verticalLayout_16.addWidget(self.label_40)

        self.leTarifaFpConsumoTe = RQLineEdit(self.groupBox_8)
        self.leTarifaFpConsumoTe.setObjectName(u"leTarifaFpConsumoTe")

        self.verticalLayout_16.addWidget(self.leTarifaFpConsumoTe)


        self.gridLayout_8.addLayout(self.verticalLayout_16, 1, 0, 1, 1)

        self.verticalLayout_17 = QVBoxLayout()
        self.verticalLayout_17.setObjectName(u"verticalLayout_17")
        self.label_41 = QLabel(self.groupBox_8)
        self.label_41.setObjectName(u"label_41")

        self.verticalLayout_17.addWidget(self.label_41)

        self.leTarifaFpConsumoTusd = RQLineEdit(self.groupBox_8)
        self.leTarifaFpConsumoTusd.setObjectName(u"leTarifaFpConsumoTusd")

        self.verticalLayout_17.addWidget(self.leTarifaFpConsumoTusd)


        self.gridLayout_8.addLayout(self.verticalLayout_17, 1, 1, 1, 1)

        self.verticalLayout_18 = QVBoxLayout()
        self.verticalLayout_18.setSpacing(6)
        self.verticalLayout_18.setObjectName(u"verticalLayout_18")
        self.label_42 = QLabel(self.groupBox_8)
        self.label_42.setObjectName(u"label_42")

        self.verticalLayout_18.addWidget(self.label_42)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.leTarifaFpConsumo = RQLineEdit(self.groupBox_8)
        self.leTarifaFpConsumo.setObjectName(u"leTarifaFpConsumo")

        self.horizontalLayout_4.addWidget(self.leTarifaFpConsumo)

        self.pbSomarConsumoFp = QPushButton(self.groupBox_8)
        self.pbSomarConsumoFp.setObjectName(u"pbSomarConsumoFp")
        self.pbSomarConsumoFp.setMaximumSize(QSize(19, 20))
        self.pbSomarConsumoFp.setAutoDefault(False)

        self.horizontalLayout_4.addWidget(self.pbSomarConsumoFp)


        self.verticalLayout_18.addLayout(self.horizontalLayout_4)


        self.gridLayout_8.addLayout(self.verticalLayout_18, 1, 2, 1, 1)

        self.verticalLayout_19 = QVBoxLayout()
        self.verticalLayout_19.setObjectName(u"verticalLayout_19")
        self.label_43 = QLabel(self.groupBox_8)
        self.label_43.setObjectName(u"label_43")

        self.verticalLayout_19.addWidget(self.label_43)

        self.leTarifaFpUfer = RQLineEdit(self.groupBox_8)
        self.leTarifaFpUfer.setObjectName(u"leTarifaFpUfer")

        self.verticalLayout_19.addWidget(self.leTarifaFpUfer)


        self.gridLayout_8.addLayout(self.verticalLayout_19, 2, 0, 1, 1)


        self.gridLayout_11.addWidget(self.groupBox_8, 0, 1, 1, 1)

        self.groupBox_9 = QGroupBox(self.tab_2)
        self.groupBox_9.setObjectName(u"groupBox_9")
        self.gridLayout_10 = QGridLayout(self.groupBox_9)
        self.gridLayout_10.setObjectName(u"gridLayout_10")
        self.verticalLayout_9 = QVBoxLayout()
        self.verticalLayout_9.setObjectName(u"verticalLayout_9")
        self.label_47 = QLabel(self.groupBox_9)
        self.label_47.setObjectName(u"label_47")

        self.verticalLayout_9.addWidget(self.label_47)

        self.leTarifaHrConsumoTe = RQLineEdit(self.groupBox_9)
        self.leTarifaHrConsumoTe.setObjectName(u"leTarifaHrConsumoTe")

        self.verticalLayout_9.addWidget(self.leTarifaHrConsumoTe)


        self.gridLayout_10.addLayout(self.verticalLayout_9, 0, 0, 1, 1)

        self.verticalLayout_10 = QVBoxLayout()
        self.verticalLayout_10.setObjectName(u"verticalLayout_10")
        self.label_48 = QLabel(self.groupBox_9)
        self.label_48.setObjectName(u"label_48")

        self.verticalLayout_10.addWidget(self.label_48)

        self.leTarifaHrConsumoTusd = RQLineEdit(self.groupBox_9)
        self.leTarifaHrConsumoTusd.setObjectName(u"leTarifaHrConsumoTusd")

        self.verticalLayout_10.addWidget(self.leTarifaHrConsumoTusd)


        self.gridLayout_10.addLayout(self.verticalLayout_10, 0, 1, 1, 1)

        self.verticalLayout_11 = QVBoxLayout()
        self.verticalLayout_11.setObjectName(u"verticalLayout_11")
        self.label_50 = QLabel(self.groupBox_9)
        self.label_50.setObjectName(u"label_50")

        self.verticalLayout_11.addWidget(self.label_50)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.leTarifaHrConsumo = RQLineEdit(self.groupBox_9)
        self.leTarifaHrConsumo.setObjectName(u"leTarifaHrConsumo")

        self.horizontalLayout_3.addWidget(self.leTarifaHrConsumo)

        self.pbSomarConsumoHr = QPushButton(self.groupBox_9)
        self.pbSomarConsumoHr.setObjectName(u"pbSomarConsumoHr")
        self.pbSomarConsumoHr.setMaximumSize(QSize(19, 20))
        self.pbSomarConsumoHr.setAutoDefault(False)

        self.horizontalLayout_3.addWidget(self.pbSomarConsumoHr)


        self.verticalLayout_11.addLayout(self.horizontalLayout_3)


        self.gridLayout_10.addLayout(self.verticalLayout_11, 0, 2, 1, 1)

        self.verticalLayout_12 = QVBoxLayout()
        self.verticalLayout_12.setObjectName(u"verticalLayout_12")
        self.label_49 = QLabel(self.groupBox_9)
        self.label_49.setObjectName(u"label_49")

        self.verticalLayout_12.addWidget(self.label_49)

        self.leTarifaHrUfer = RQLineEdit(self.groupBox_9)
        self.leTarifaHrUfer.setObjectName(u"leTarifaHrUfer")

        self.verticalLayout_12.addWidget(self.leTarifaHrUfer)


        self.gridLayout_10.addLayout(self.verticalLayout_12, 1, 0, 1, 1)


        self.gridLayout_11.addWidget(self.groupBox_9, 1, 0, 1, 1)

        self.groupBox_10 = QGroupBox(self.tab_2)
        self.groupBox_10.setObjectName(u"groupBox_10")
        self.gridLayout_9 = QGridLayout(self.groupBox_10)
        self.gridLayout_9.setObjectName(u"gridLayout_9")
        self.label_51 = QLabel(self.groupBox_10)
        self.label_51.setObjectName(u"label_51")

        self.gridLayout_9.addWidget(self.label_51, 0, 0, 1, 1)

        self.label_54 = QLabel(self.groupBox_10)
        self.label_54.setObjectName(u"label_54")

        self.gridLayout_9.addWidget(self.label_54, 0, 2, 1, 1)

        self.leImpostoCofins = RQLineEdit(self.groupBox_10)
        self.leImpostoCofins.setObjectName(u"leImpostoCofins")

        self.gridLayout_9.addWidget(self.leImpostoCofins, 1, 1, 1, 1)

        self.label_52 = QLabel(self.groupBox_10)
        self.label_52.setObjectName(u"label_52")

        self.gridLayout_9.addWidget(self.label_52, 0, 1, 1, 1)

        self.leImpostoIcms = RQLineEdit(self.groupBox_10)
        self.leImpostoIcms.setObjectName(u"leImpostoIcms")

        self.gridLayout_9.addWidget(self.leImpostoIcms, 1, 2, 1, 1)

        self.leImpostoPis = RQLineEdit(self.groupBox_10)
        self.leImpostoPis.setObjectName(u"leImpostoPis")

        self.gridLayout_9.addWidget(self.leImpostoPis, 1, 0, 1, 1)

        self.verticalSpacer_4 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_9.addItem(self.verticalSpacer_4, 2, 0, 1, 1)


        self.gridLayout_11.addWidget(self.groupBox_10, 1, 1, 1, 1)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_11.addItem(self.verticalSpacer, 2, 0, 1, 2)

        self.tabWidget.addTab(self.tab_2, "")
        self.tab_3 = QWidget()
        self.tab_3.setObjectName(u"tab_3")
        self.gridLayout_12 = QGridLayout(self.tab_3)
        self.gridLayout_12.setObjectName(u"gridLayout_12")
        self.leCompensacaoDescricao = QLineEdit(self.tab_3)
        self.leCompensacaoDescricao.setObjectName(u"leCompensacaoDescricao")
        self.leCompensacaoDescricao.setMinimumSize(QSize(350, 0))

        self.gridLayout_12.addWidget(self.leCompensacaoDescricao, 1, 0, 1, 1)

        self.label_45 = QLabel(self.tab_3)
        self.label_45.setObjectName(u"label_45")

        self.gridLayout_12.addWidget(self.label_45, 0, 1, 1, 1)

        self.leCompensacaoValor = QLineEdit(self.tab_3)
        self.leCompensacaoValor.setObjectName(u"leCompensacaoValor")

        self.gridLayout_12.addWidget(self.leCompensacaoValor, 1, 1, 1, 1)

        self.label_44 = QLabel(self.tab_3)
        self.label_44.setObjectName(u"label_44")

        self.gridLayout_12.addWidget(self.label_44, 0, 0, 1, 1)

        self.pbAdicionarCompensacao = QPushButton(self.tab_3)
        self.pbAdicionarCompensacao.setObjectName(u"pbAdicionarCompensacao")

        self.gridLayout_12.addWidget(self.pbAdicionarCompensacao, 1, 2, 1, 2)

        self.twCompensacoes = QTableWidget(self.tab_3)
        if (self.twCompensacoes.columnCount() < 2):
            self.twCompensacoes.setColumnCount(2)
        __qtablewidgetitem = QTableWidgetItem()
        self.twCompensacoes.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        self.twCompensacoes.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        self.twCompensacoes.setObjectName(u"twCompensacoes")
        self.twCompensacoes.horizontalHeader().setHighlightSections(False)
        self.twCompensacoes.verticalHeader().setDefaultSectionSize(23)

        self.gridLayout_12.addWidget(self.twCompensacoes, 3, 0, 1, 4)

        self.label_20 = QLabel(self.tab_3)
        self.label_20.setObjectName(u"label_20")

        self.gridLayout_12.addWidget(self.label_20, 2, 0, 1, 4)

        self.tabWidget.addTab(self.tab_3, "")
        self.tabValores = QWidget()
        self.tabValores.setObjectName(u"tabValores")
        self.gridLayout_19 = QGridLayout(self.tabValores)
        self.gridLayout_19.setObjectName(u"gridLayout_19")
        self.groupBox_13 = QGroupBox(self.tabValores)
        self.groupBox_13.setObjectName(u"groupBox_13")
        self.gridLayout_17 = QGridLayout(self.groupBox_13)
        self.gridLayout_17.setObjectName(u"gridLayout_17")
        self.label_76 = QLabel(self.groupBox_13)
        self.label_76.setObjectName(u"label_76")

        self.gridLayout_17.addWidget(self.label_76, 0, 0, 1, 1)

        self.label_75 = QLabel(self.groupBox_13)
        self.label_75.setObjectName(u"label_75")

        self.gridLayout_17.addWidget(self.label_75, 0, 1, 1, 1)

        self.leValoresHrConsumo = QLineEdit(self.groupBox_13)
        self.leValoresHrConsumo.setObjectName(u"leValoresHrConsumo")

        self.gridLayout_17.addWidget(self.leValoresHrConsumo, 1, 0, 1, 1)

        self.leValoresHrUfer = QLineEdit(self.groupBox_13)
        self.leValoresHrUfer.setObjectName(u"leValoresHrUfer")

        self.gridLayout_17.addWidget(self.leValoresHrUfer, 1, 1, 1, 1)


        self.gridLayout_19.addWidget(self.groupBox_13, 1, 0, 1, 1)

        self.groupBox_11 = QGroupBox(self.tabValores)
        self.groupBox_11.setObjectName(u"groupBox_11")
        self.gridLayout_13 = QGridLayout(self.groupBox_11)
        self.gridLayout_13.setObjectName(u"gridLayout_13")
        self.label_46 = QLabel(self.groupBox_11)
        self.label_46.setObjectName(u"label_46")

        self.gridLayout_13.addWidget(self.label_46, 0, 0, 1, 1)

        self.label_53 = QLabel(self.groupBox_11)
        self.label_53.setObjectName(u"label_53")

        self.gridLayout_13.addWidget(self.label_53, 0, 1, 1, 1)

        self.leValoresPDemanda = QLineEdit(self.groupBox_11)
        self.leValoresPDemanda.setObjectName(u"leValoresPDemanda")

        self.gridLayout_13.addWidget(self.leValoresPDemanda, 1, 0, 1, 1)

        self.leValoresPDemandaUltrapassagem = QLineEdit(self.groupBox_11)
        self.leValoresPDemandaUltrapassagem.setObjectName(u"leValoresPDemandaUltrapassagem")

        self.gridLayout_13.addWidget(self.leValoresPDemandaUltrapassagem, 1, 1, 1, 1)

        self.label_55 = QLabel(self.groupBox_11)
        self.label_55.setObjectName(u"label_55")

        self.gridLayout_13.addWidget(self.label_55, 2, 0, 1, 1)

        self.label_56 = QLabel(self.groupBox_11)
        self.label_56.setObjectName(u"label_56")

        self.gridLayout_13.addWidget(self.label_56, 2, 1, 1, 1)

        self.leValoresPConsumo = QLineEdit(self.groupBox_11)
        self.leValoresPConsumo.setObjectName(u"leValoresPConsumo")

        self.gridLayout_13.addWidget(self.leValoresPConsumo, 3, 0, 1, 1)

        self.leValoresPUfer = QLineEdit(self.groupBox_11)
        self.leValoresPUfer.setObjectName(u"leValoresPUfer")

        self.gridLayout_13.addWidget(self.leValoresPUfer, 3, 1, 1, 1)

        self.label_57 = QLabel(self.groupBox_11)
        self.label_57.setObjectName(u"label_57")

        self.gridLayout_13.addWidget(self.label_57, 4, 0, 1, 1)

        self.leValoresPDmcr = QLineEdit(self.groupBox_11)
        self.leValoresPDmcr.setObjectName(u"leValoresPDmcr")

        self.gridLayout_13.addWidget(self.leValoresPDmcr, 5, 0, 1, 1)


        self.gridLayout_19.addWidget(self.groupBox_11, 0, 0, 1, 1)

        self.pbCalcularValores = QPushButton(self.tabValores)
        self.pbCalcularValores.setObjectName(u"pbCalcularValores")

        self.gridLayout_19.addWidget(self.pbCalcularValores, 2, 1, 1, 1)

        self.groupBox_12 = QGroupBox(self.tabValores)
        self.groupBox_12.setObjectName(u"groupBox_12")
        self.gridLayout_14 = QGridLayout(self.groupBox_12)
        self.gridLayout_14.setObjectName(u"gridLayout_14")
        self.leValoresFpDemanda = QLineEdit(self.groupBox_12)
        self.leValoresFpDemanda.setObjectName(u"leValoresFpDemanda")

        self.gridLayout_14.addWidget(self.leValoresFpDemanda, 1, 0, 1, 1)

        self.leValoresFpDemandaUltrapassagem = QLineEdit(self.groupBox_12)
        self.leValoresFpDemandaUltrapassagem.setObjectName(u"leValoresFpDemandaUltrapassagem")

        self.gridLayout_14.addWidget(self.leValoresFpDemandaUltrapassagem, 1, 1, 1, 1)

        self.label_59 = QLabel(self.groupBox_12)
        self.label_59.setObjectName(u"label_59")

        self.gridLayout_14.addWidget(self.label_59, 0, 1, 1, 1)

        self.leValoresFpUfer = QLineEdit(self.groupBox_12)
        self.leValoresFpUfer.setObjectName(u"leValoresFpUfer")

        self.gridLayout_14.addWidget(self.leValoresFpUfer, 3, 1, 1, 1)

        self.leValoresFpDmcr = QLineEdit(self.groupBox_12)
        self.leValoresFpDmcr.setObjectName(u"leValoresFpDmcr")

        self.gridLayout_14.addWidget(self.leValoresFpDmcr, 5, 0, 1, 1)

        self.label_58 = QLabel(self.groupBox_12)
        self.label_58.setObjectName(u"label_58")

        self.gridLayout_14.addWidget(self.label_58, 0, 0, 1, 1)

        self.label_61 = QLabel(self.groupBox_12)
        self.label_61.setObjectName(u"label_61")

        self.gridLayout_14.addWidget(self.label_61, 2, 1, 1, 1)

        self.leValoresFpConsumo = QLineEdit(self.groupBox_12)
        self.leValoresFpConsumo.setObjectName(u"leValoresFpConsumo")

        self.gridLayout_14.addWidget(self.leValoresFpConsumo, 3, 0, 1, 1)

        self.label_60 = QLabel(self.groupBox_12)
        self.label_60.setObjectName(u"label_60")

        self.gridLayout_14.addWidget(self.label_60, 2, 0, 1, 1)

        self.label_62 = QLabel(self.groupBox_12)
        self.label_62.setObjectName(u"label_62")

        self.gridLayout_14.addWidget(self.label_62, 4, 0, 1, 1)


        self.gridLayout_19.addWidget(self.groupBox_12, 0, 1, 1, 1)

        self.horizontalSpacer = QSpacerItem(278, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.gridLayout_19.addItem(self.horizontalSpacer, 2, 0, 1, 1)

        self.groupBox_14 = QGroupBox(self.tabValores)
        self.groupBox_14.setObjectName(u"groupBox_14")
        self.gridLayout_18 = QGridLayout(self.groupBox_14)
        self.gridLayout_18.setObjectName(u"gridLayout_18")
        self.leValoresCompensacoes = QLineEdit(self.groupBox_14)
        self.leValoresCompensacoes.setObjectName(u"leValoresCompensacoes")

        self.gridLayout_18.addWidget(self.leValoresCompensacoes, 1, 0, 1, 1)

        self.label_77 = QLabel(self.groupBox_14)
        self.label_77.setObjectName(u"label_77")

        self.gridLayout_18.addWidget(self.label_77, 0, 0, 1, 1)


        self.gridLayout_19.addWidget(self.groupBox_14, 1, 1, 1, 1)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.gridLayout_19.addItem(self.verticalSpacer_2, 3, 1, 1, 1)

        self.tabWidget.addTab(self.tabValores, "")

        self.verticalLayout.addWidget(self.tabWidget)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer_2)

        self.pbCancelar = QPushButton(frmCadastroFatura)
        self.pbCancelar.setObjectName(u"pbCancelar")

        self.horizontalLayout.addWidget(self.pbCancelar)

        self.pbSalvar = QPushButton(frmCadastroFatura)
        self.pbSalvar.setObjectName(u"pbSalvar")

        self.horizontalLayout.addWidget(self.pbSalvar)


        self.verticalLayout.addLayout(self.horizontalLayout)

        QWidget.setTabOrder(self.cbTipoCliente, self.cbModalidade)
        QWidget.setTabOrder(self.cbModalidade, self.deLeituraAnterior)
        QWidget.setTabOrder(self.deLeituraAnterior, self.deLeituraAtual)
        QWidget.setTabOrder(self.deLeituraAtual, self.deProximaLeitura)
        QWidget.setTabOrder(self.deProximaLeitura, self.leDiasFaturado)
        QWidget.setTabOrder(self.leDiasFaturado, self.deMesReferencia)
        QWidget.setTabOrder(self.deMesReferencia, self.deVencimento)
        QWidget.setTabOrder(self.deVencimento, self.leValor)
        QWidget.setTabOrder(self.leValor, self.leMedidor)
        QWidget.setTabOrder(self.leMedidor, self.deApresentacao)
        QWidget.setTabOrder(self.deApresentacao, self.leDemandaContratadaFp)
        QWidget.setTabOrder(self.leDemandaContratadaFp, self.leDemandaContratadaP)
        QWidget.setTabOrder(self.leDemandaContratadaP, self.lePerdaTransformador)
        QWidget.setTabOrder(self.lePerdaTransformador, self.tabWidget)
        QWidget.setTabOrder(self.tabWidget, self.leProdutosDemandaFaturadaP)
        QWidget.setTabOrder(self.leProdutosDemandaFaturadaP, self.leProdutosDemandaFaturadaFp)
        QWidget.setTabOrder(self.leProdutosDemandaFaturadaFp, self.leProdutosDmcrP)
        QWidget.setTabOrder(self.leProdutosDmcrP, self.leProdutosDmcrFp)
        QWidget.setTabOrder(self.leProdutosDmcrFp, self.leProdutosDmcrHr)
        QWidget.setTabOrder(self.leProdutosDmcrHr, self.leProdutosConsumoP)
        QWidget.setTabOrder(self.leProdutosConsumoP, self.leProdutosConsumoFp)
        QWidget.setTabOrder(self.leProdutosConsumoFp, self.leProdutosConsumoHr)
        QWidget.setTabOrder(self.leProdutosConsumoHr, self.leProdutosUferP)
        QWidget.setTabOrder(self.leProdutosUferP, self.leProdutosUferFp)
        QWidget.setTabOrder(self.leProdutosUferFp, self.leProdutosUferHr)
        QWidget.setTabOrder(self.leProdutosUferHr, self.leTarifaPDemanda)
        QWidget.setTabOrder(self.leTarifaPDemanda, self.leTarifaPDemandaUltrapassagem)
        QWidget.setTabOrder(self.leTarifaPDemandaUltrapassagem, self.leTarifaPDmcr)
        QWidget.setTabOrder(self.leTarifaPDmcr, self.leTarifaPConsumoTe)
        QWidget.setTabOrder(self.leTarifaPConsumoTe, self.leTarifaPConsumoTusd)
        QWidget.setTabOrder(self.leTarifaPConsumoTusd, self.leTarifaPConsumo)
        QWidget.setTabOrder(self.leTarifaPConsumo, self.leTarifaPUfer)
        QWidget.setTabOrder(self.leTarifaPUfer, self.leTarifaFpDemanda)
        QWidget.setTabOrder(self.leTarifaFpDemanda, self.leTarifaFpDemandaUltrapassagem)
        QWidget.setTabOrder(self.leTarifaFpDemandaUltrapassagem, self.leTarifaFpDmcr)
        QWidget.setTabOrder(self.leTarifaFpDmcr, self.leTarifaFpConsumoTe)
        QWidget.setTabOrder(self.leTarifaFpConsumoTe, self.leTarifaFpConsumoTusd)
        QWidget.setTabOrder(self.leTarifaFpConsumoTusd, self.leTarifaFpConsumo)
        QWidget.setTabOrder(self.leTarifaFpConsumo, self.leTarifaFpUfer)
        QWidget.setTabOrder(self.leTarifaFpUfer, self.leTarifaHrConsumoTe)
        QWidget.setTabOrder(self.leTarifaHrConsumoTe, self.leTarifaHrConsumoTusd)
        QWidget.setTabOrder(self.leTarifaHrConsumoTusd, self.leTarifaHrConsumo)
        QWidget.setTabOrder(self.leTarifaHrConsumo, self.leTarifaHrUfer)
        QWidget.setTabOrder(self.leTarifaHrUfer, self.leImpostoPis)
        QWidget.setTabOrder(self.leImpostoPis, self.leImpostoCofins)
        QWidget.setTabOrder(self.leImpostoCofins, self.leImpostoIcms)
        QWidget.setTabOrder(self.leImpostoIcms, self.leCompensacaoDescricao)
        QWidget.setTabOrder(self.leCompensacaoDescricao, self.leCompensacaoValor)
        QWidget.setTabOrder(self.leCompensacaoValor, self.pbAdicionarCompensacao)
        QWidget.setTabOrder(self.pbAdicionarCompensacao, self.twCompensacoes)
        QWidget.setTabOrder(self.twCompensacoes, self.leValoresPDemanda)
        QWidget.setTabOrder(self.leValoresPDemanda, self.leValoresPDemandaUltrapassagem)
        QWidget.setTabOrder(self.leValoresPDemandaUltrapassagem, self.leValoresPConsumo)
        QWidget.setTabOrder(self.leValoresPConsumo, self.leValoresPUfer)
        QWidget.setTabOrder(self.leValoresPUfer, self.leValoresPDmcr)
        QWidget.setTabOrder(self.leValoresPDmcr, self.leValoresFpDemanda)
        QWidget.setTabOrder(self.leValoresFpDemanda, self.leValoresFpDemandaUltrapassagem)
        QWidget.setTabOrder(self.leValoresFpDemandaUltrapassagem, self.leValoresFpConsumo)
        QWidget.setTabOrder(self.leValoresFpConsumo, self.leValoresFpUfer)
        QWidget.setTabOrder(self.leValoresFpUfer, self.leValoresFpDmcr)
        QWidget.setTabOrder(self.leValoresFpDmcr, self.leValoresHrConsumo)
        QWidget.setTabOrder(self.leValoresHrConsumo, self.leValoresHrUfer)
        QWidget.setTabOrder(self.leValoresHrUfer, self.leValoresCompensacoes)
        QWidget.setTabOrder(self.leValoresCompensacoes, self.pbCalcularValores)
        QWidget.setTabOrder(self.pbCalcularValores, self.pbCancelar)
        QWidget.setTabOrder(self.pbCancelar, self.pbSalvar)

        self.retranslateUi(frmCadastroFatura)

        self.cbModalidade.setCurrentIndex(-1)
        self.cbTipoCliente.setCurrentIndex(-1)
        self.tabWidget.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(frmCadastroFatura)
    # setupUi

    def retranslateUi(self, frmCadastroFatura):
        frmCadastroFatura.setWindowTitle(QCoreApplication.translate("frmCadastroFatura", u"Formul\u00e1rio Fatura", None))
        self.label_13.setText(QCoreApplication.translate("frmCadastroFatura", u"Modalidade", None))
        self.label_14.setText(QCoreApplication.translate("frmCadastroFatura", u"Tipo Cliente", None))
        self.cbModalidade.setItemText(0, QCoreApplication.translate("frmCadastroFatura", u"VERDE", None))
        self.cbModalidade.setItemText(1, QCoreApplication.translate("frmCadastroFatura", u"AZUL", None))

        self.cbTipoCliente.setItemText(0, QCoreApplication.translate("frmCadastroFatura", u"SAZONAL", None))
        self.cbTipoCliente.setItemText(1, QCoreApplication.translate("frmCadastroFatura", u"INDUSTRIAL", None))

        self.label.setText(QCoreApplication.translate("frmCadastroFatura", u"Leitura Anterior", None))
        self.label_2.setText(QCoreApplication.translate("frmCadastroFatura", u"Leitura Atual", None))
        self.label_3.setText(QCoreApplication.translate("frmCadastroFatura", u"Pr\u00f3xima Leitura", None))
        self.label_4.setText(QCoreApplication.translate("frmCadastroFatura", u"Dias Faturado", None))
        self.label_6.setText(QCoreApplication.translate("frmCadastroFatura", u"M\u00eas Refer\u00eancia", None))
        self.label_8.setText(QCoreApplication.translate("frmCadastroFatura", u"Vencimento", None))
        self.label_7.setText(QCoreApplication.translate("frmCadastroFatura", u"Valor", None))
        self.label_5.setText(QCoreApplication.translate("frmCadastroFatura", u"Medidor", None))
        self.deMesReferencia.setDisplayFormat(QCoreApplication.translate("frmCadastroFatura", u"MMM/yyyy", None))
        self.leValor.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_11.setText(QCoreApplication.translate("frmCadastroFatura", u"Apresenta\u00e7\u00e3o", None))
        self.label_10.setText(QCoreApplication.translate("frmCadastroFatura", u"Demanda F.P", None))
        self.label_12.setText(QCoreApplication.translate("frmCadastroFatura", u"Demanda P.", None))
        self.label_9.setText(QCoreApplication.translate("frmCadastroFatura", u"Perda do Transformador", None))
        self.leDemandaContratadaFp.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leDemandaContratadaP.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.lePerdaTransformador.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_6.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Demanda Registrada", None))
        self.label_64.setText(QCoreApplication.translate("frmCadastroFatura", u"P.", None))
        self.label_65.setText(QCoreApplication.translate("frmCadastroFatura", u"F.P", None))
        self.label_66.setText(QCoreApplication.translate("frmCadastroFatura", u"H.R", None))
        self.leProdutosDemandaRegistradaP.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosDemandaRegistradaFp.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosDemandaRegistradaHr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Demanda Faturada", None))
        self.label_18.setText(QCoreApplication.translate("frmCadastroFatura", u"P.", None))
        self.label_19.setText(QCoreApplication.translate("frmCadastroFatura", u"F.P", None))
        self.label_27.setText(QCoreApplication.translate("frmCadastroFatura", u"H.R", None))
        self.leProdutosDemandaFaturadaP.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosDemandaFaturadaFp.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosDemandaFaturadaHr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_5.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.label_24.setText(QCoreApplication.translate("frmCadastroFatura", u"P.", None))
        self.label_25.setText(QCoreApplication.translate("frmCadastroFatura", u"F.P", None))
        self.label_26.setText(QCoreApplication.translate("frmCadastroFatura", u"H.R", None))
        self.leProdutosConsumoP.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosConsumoFp.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosConsumoHr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_15.setTitle(QCoreApplication.translate("frmCadastroFatura", u"DMCR", None))
        self.label_73.setText(QCoreApplication.translate("frmCadastroFatura", u"P.", None))
        self.label_74.setText(QCoreApplication.translate("frmCadastroFatura", u"F.P", None))
        self.label_79.setText(QCoreApplication.translate("frmCadastroFatura", u"H.R", None))
        self.leProdutosDmcrP.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosDmcrFp.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosDmcrHr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.label_21.setText(QCoreApplication.translate("frmCadastroFatura", u"P.", None))
        self.label_22.setText(QCoreApplication.translate("frmCadastroFatura", u"F.P", None))
        self.label_23.setText(QCoreApplication.translate("frmCadastroFatura", u"H.R", None))
        self.leProdutosUferP.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosUferFp.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leProdutosUferHr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), QCoreApplication.translate("frmCadastroFatura", u"Produtos", None))
        self.groupBox_7.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Tarifas P.", None))
        self.label_30.setText(QCoreApplication.translate("frmCadastroFatura", u"Demanda", None))
        self.leTarifaPDemanda.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_31.setText(QCoreApplication.translate("frmCadastroFatura", u"Ultrapassagem D.", None))
        self.leTarifaPDemandaUltrapassagem.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_37.setText(QCoreApplication.translate("frmCadastroFatura", u"DMCR", None))
        self.leTarifaPDmcr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_34.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo TE", None))
        self.leTarifaPConsumoTe.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_35.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo TUSD", None))
        self.leTarifaPConsumoTusd.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_33.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.leTarifaPConsumo.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
#if QT_CONFIG(tooltip)
        self.pbSomarConsumoP.setToolTip(QCoreApplication.translate("frmCadastroFatura", u"Somar Te + TUSD", None))
#endif // QT_CONFIG(tooltip)
        self.pbSomarConsumoP.setText(QCoreApplication.translate("frmCadastroFatura", u"+", None))
        self.label_36.setText(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.leTarifaPUfer.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_8.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Tarifas F.P.", None))
        self.label_32.setText(QCoreApplication.translate("frmCadastroFatura", u"Demanda", None))
        self.leTarifaFpDemanda.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_38.setText(QCoreApplication.translate("frmCadastroFatura", u"Ultrapassagem D.", None))
        self.leTarifaFpDemandaUltrapassagem.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_39.setText(QCoreApplication.translate("frmCadastroFatura", u"DMCR", None))
        self.leTarifaFpDmcr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_40.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo TE", None))
        self.leTarifaFpConsumoTe.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_41.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo TUSD", None))
        self.leTarifaFpConsumoTusd.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_42.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.leTarifaFpConsumo.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
#if QT_CONFIG(tooltip)
        self.pbSomarConsumoFp.setToolTip(QCoreApplication.translate("frmCadastroFatura", u"Somar Te + TUSD", None))
#endif // QT_CONFIG(tooltip)
        self.pbSomarConsumoFp.setText(QCoreApplication.translate("frmCadastroFatura", u"+", None))
        self.label_43.setText(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.leTarifaFpUfer.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_9.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Tarifas H.R.", None))
        self.label_47.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo TE", None))
        self.leTarifaHrConsumoTe.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_48.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo TUSD", None))
        self.leTarifaHrConsumoTusd.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_50.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.leTarifaHrConsumo.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
#if QT_CONFIG(tooltip)
        self.pbSomarConsumoHr.setToolTip(QCoreApplication.translate("frmCadastroFatura", u"Somar Te + TUSD", None))
#endif // QT_CONFIG(tooltip)
        self.pbSomarConsumoHr.setText(QCoreApplication.translate("frmCadastroFatura", u"+", None))
        self.label_49.setText(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.leTarifaHrUfer.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_10.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Impostos", None))
        self.label_51.setText(QCoreApplication.translate("frmCadastroFatura", u"PIS", None))
        self.label_54.setText(QCoreApplication.translate("frmCadastroFatura", u"ICMS", None))
        self.leImpostoCofins.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_52.setText(QCoreApplication.translate("frmCadastroFatura", u"COFINS", None))
        self.leImpostoIcms.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leImpostoPis.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), QCoreApplication.translate("frmCadastroFatura", u"Tarifas && Impostos", None))
        self.label_45.setText(QCoreApplication.translate("frmCadastroFatura", u"Valor", None))
        self.leCompensacaoValor.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_44.setText(QCoreApplication.translate("frmCadastroFatura", u"Descri\u00e7\u00e3o", None))
        self.pbAdicionarCompensacao.setText(QCoreApplication.translate("frmCadastroFatura", u"Adicionar", None))
        ___qtablewidgetitem = self.twCompensacoes.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("frmCadastroFatura", u"Descri\u00e7\u00e3o", None));
        ___qtablewidgetitem1 = self.twCompensacoes.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("frmCadastroFatura", u"Valor", None));
        self.label_20.setText(QCoreApplication.translate("frmCadastroFatura", u"<html><head/><body><p><span style=\" color:#616161;\">Pressione DELETE para remover:</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), QCoreApplication.translate("frmCadastroFatura", u"Compensa\u00e7\u00f5es/Devolu\u00e7\u00f5es", None))
        self.groupBox_13.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Valores H.R.", None))
        self.label_76.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.label_75.setText(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.leValoresHrConsumo.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leValoresHrUfer.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.groupBox_11.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Valores P.", None))
        self.label_46.setText(QCoreApplication.translate("frmCadastroFatura", u"Demanda", None))
        self.label_53.setText(QCoreApplication.translate("frmCadastroFatura", u"Ultrapassagem D.", None))
        self.leValoresPDemanda.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leValoresPDemandaUltrapassagem.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_55.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.label_56.setText(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.leValoresPConsumo.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leValoresPUfer.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_57.setText(QCoreApplication.translate("frmCadastroFatura", u"DMCR", None))
        self.leValoresPDmcr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.pbCalcularValores.setText(QCoreApplication.translate("frmCadastroFatura", u"Calcular Valores", None))
        self.groupBox_12.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Valores F.P.", None))
        self.leValoresFpDemanda.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leValoresFpDemandaUltrapassagem.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_59.setText(QCoreApplication.translate("frmCadastroFatura", u"Ultrapassagem D.", None))
        self.leValoresFpUfer.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.leValoresFpDmcr.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_58.setText(QCoreApplication.translate("frmCadastroFatura", u"Demanda", None))
        self.label_61.setText(QCoreApplication.translate("frmCadastroFatura", u"UFER", None))
        self.leValoresFpConsumo.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_60.setText(QCoreApplication.translate("frmCadastroFatura", u"Consumo", None))
        self.label_62.setText(QCoreApplication.translate("frmCadastroFatura", u"DMCR", None))
        self.groupBox_14.setTitle(QCoreApplication.translate("frmCadastroFatura", u"Devolu\u00e7\u00f5es/Compensa\u00e7\u00f5es", None))
        self.leValoresCompensacoes.setText(QCoreApplication.translate("frmCadastroFatura", u"0,0", None))
        self.label_77.setText(QCoreApplication.translate("frmCadastroFatura", u"Total", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tabValores), QCoreApplication.translate("frmCadastroFatura", u"Valores", None))
        self.pbCancelar.setText(QCoreApplication.translate("frmCadastroFatura", u"Cancelar", None))
        self.pbSalvar.setText(QCoreApplication.translate("frmCadastroFatura", u"Salvar", None))
    # retranslateUi

