# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'ui_frm_unidades.ui'
##
## Created by: Qt User Interface Compiler version 5.15.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *
from PySide2.QtGui import *
from PySide2.QtWidgets import *


class Ui_frmUnidades(object):
    def setupUi(self, frmUnidades):
        if not frmUnidades.objectName():
            frmUnidades.setObjectName(u"frmUnidades")
        frmUnidades.resize(1083, 554)
        self.centralwidget = QWidget(frmUnidades)
        self.centralwidget.setObjectName(u"centralwidget")
        self.verticalLayout_3 = QVBoxLayout(self.centralwidget)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalLayout_3.setContentsMargins(9, 9, 9, 9)
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.groupBox_3 = QGroupBox(self.centralwidget)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.verticalLayout_4 = QVBoxLayout(self.groupBox_3)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.pbImportarExcel = QPushButton(self.groupBox_3)
        self.pbImportarExcel.setObjectName(u"pbImportarExcel")

        self.verticalLayout_4.addWidget(self.pbImportarExcel)


        self.horizontalLayout_2.addWidget(self.groupBox_3)

        self.groupBox = QGroupBox(self.centralwidget)
        self.groupBox.setObjectName(u"groupBox")
        self.horizontalLayout_3 = QHBoxLayout(self.groupBox)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.pbGrupos = QPushButton(self.groupBox)
        self.pbGrupos.setObjectName(u"pbGrupos")

        self.horizontalLayout_3.addWidget(self.pbGrupos)


        self.horizontalLayout_2.addWidget(self.groupBox)

        self.groupBox_4 = QGroupBox(self.centralwidget)
        self.groupBox_4.setObjectName(u"groupBox_4")
        self.verticalLayout = QVBoxLayout(self.groupBox_4)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pbCadastrarUnidade = QPushButton(self.groupBox_4)
        self.pbCadastrarUnidade.setObjectName(u"pbCadastrarUnidade")

        self.horizontalLayout.addWidget(self.pbCadastrarUnidade)

        self.pbRecarregar = QPushButton(self.groupBox_4)
        self.pbRecarregar.setObjectName(u"pbRecarregar")

        self.horizontalLayout.addWidget(self.pbRecarregar)

        self.pbFaturas = QPushButton(self.groupBox_4)
        self.pbFaturas.setObjectName(u"pbFaturas")

        self.horizontalLayout.addWidget(self.pbFaturas)

        self.pbEditar = QPushButton(self.groupBox_4)
        self.pbEditar.setObjectName(u"pbEditar")

        self.horizontalLayout.addWidget(self.pbEditar)

        self.pbDeletar = QPushButton(self.groupBox_4)
        self.pbDeletar.setObjectName(u"pbDeletar")

        self.horizontalLayout.addWidget(self.pbDeletar)

        self.lePesquisarTexto = QLineEdit(self.groupBox_4)
        self.lePesquisarTexto.setObjectName(u"lePesquisarTexto")

        self.horizontalLayout.addWidget(self.lePesquisarTexto)

        self.pbPesquisar = QPushButton(self.groupBox_4)
        self.pbPesquisar.setObjectName(u"pbPesquisar")

        self.horizontalLayout.addWidget(self.pbPesquisar)

        self.pbGerarRelatorio = QPushButton(self.groupBox_4)
        self.pbGerarRelatorio.setObjectName(u"pbGerarRelatorio")

        self.horizontalLayout.addWidget(self.pbGerarRelatorio)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.horizontalLayout_2.addWidget(self.groupBox_4)


        self.verticalLayout_3.addLayout(self.horizontalLayout_2)

        self.groupBox_2 = QGroupBox(self.centralwidget)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.twUnidades = QTableWidget(self.groupBox_2)
        if (self.twUnidades.columnCount() < 6):
            self.twUnidades.setColumnCount(6)
        __qtablewidgetitem = QTableWidgetItem()
        self.twUnidades.setHorizontalHeaderItem(0, __qtablewidgetitem)
        __qtablewidgetitem1 = QTableWidgetItem()
        __qtablewidgetitem1.setTextAlignment(Qt.AlignLeading|Qt.AlignVCenter);
        self.twUnidades.setHorizontalHeaderItem(1, __qtablewidgetitem1)
        __qtablewidgetitem2 = QTableWidgetItem()
        __qtablewidgetitem2.setTextAlignment(Qt.AlignLeading|Qt.AlignVCenter);
        self.twUnidades.setHorizontalHeaderItem(2, __qtablewidgetitem2)
        __qtablewidgetitem3 = QTableWidgetItem()
        __qtablewidgetitem3.setTextAlignment(Qt.AlignLeading|Qt.AlignVCenter);
        self.twUnidades.setHorizontalHeaderItem(3, __qtablewidgetitem3)
        __qtablewidgetitem4 = QTableWidgetItem()
        self.twUnidades.setHorizontalHeaderItem(4, __qtablewidgetitem4)
        __qtablewidgetitem5 = QTableWidgetItem()
        self.twUnidades.setHorizontalHeaderItem(5, __qtablewidgetitem5)
        self.twUnidades.setObjectName(u"twUnidades")
        self.twUnidades.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOn)
        self.twUnidades.setAutoScroll(True)
        self.twUnidades.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.twUnidades.setSelectionMode(QAbstractItemView.SingleSelection)
        self.twUnidades.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.twUnidades.horizontalHeader().setHighlightSections(False)
        self.twUnidades.horizontalHeader().setStretchLastSection(False)
        self.twUnidades.verticalHeader().setVisible(False)
        self.twUnidades.verticalHeader().setDefaultSectionSize(23)

        self.verticalLayout_2.addWidget(self.twUnidades)


        self.verticalLayout_3.addWidget(self.groupBox_2)

        frmUnidades.setCentralWidget(self.centralwidget)
        QWidget.setTabOrder(self.pbGrupos, self.pbFaturas)
        QWidget.setTabOrder(self.pbFaturas, self.pbEditar)
        QWidget.setTabOrder(self.pbEditar, self.pbDeletar)
        QWidget.setTabOrder(self.pbDeletar, self.lePesquisarTexto)
        QWidget.setTabOrder(self.lePesquisarTexto, self.pbPesquisar)
        QWidget.setTabOrder(self.pbPesquisar, self.twUnidades)

        self.retranslateUi(frmUnidades)

        QMetaObject.connectSlotsByName(frmUnidades)
    # setupUi

    def retranslateUi(self, frmUnidades):
        frmUnidades.setWindowTitle(QCoreApplication.translate("frmUnidades", u"Unidades", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("frmUnidades", u"Excel", None))
        self.pbImportarExcel.setText(QCoreApplication.translate("frmUnidades", u"Importar", None))
        self.groupBox.setTitle(QCoreApplication.translate("frmUnidades", u"Grupos", None))
        self.pbGrupos.setText(QCoreApplication.translate("frmUnidades", u"Grupos", None))
        self.groupBox_4.setTitle(QCoreApplication.translate("frmUnidades", u"Unidades", None))
        self.pbCadastrarUnidade.setText(QCoreApplication.translate("frmUnidades", u"Cadastro", None))
        self.pbRecarregar.setText(QCoreApplication.translate("frmUnidades", u"Recarregar", None))
        self.pbFaturas.setText(QCoreApplication.translate("frmUnidades", u"Faturas", None))
        self.pbEditar.setText(QCoreApplication.translate("frmUnidades", u"Editar", None))
        self.pbDeletar.setText(QCoreApplication.translate("frmUnidades", u"Deletar", None))
        self.lePesquisarTexto.setText("")
        self.pbPesquisar.setText(QCoreApplication.translate("frmUnidades", u"Pesquisar", None))
        self.pbGerarRelatorio.setText(QCoreApplication.translate("frmUnidades", u"Relatorio", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("frmUnidades", u"Unidades", None))
        ___qtablewidgetitem = self.twUnidades.horizontalHeaderItem(0)
        ___qtablewidgetitem.setText(QCoreApplication.translate("frmUnidades", u"Id", None));
        ___qtablewidgetitem1 = self.twUnidades.horizontalHeaderItem(1)
        ___qtablewidgetitem1.setText(QCoreApplication.translate("frmUnidades", u"Grupo", None));
        ___qtablewidgetitem2 = self.twUnidades.horizontalHeaderItem(2)
        ___qtablewidgetitem2.setText(QCoreApplication.translate("frmUnidades", u"Nome", None));
        ___qtablewidgetitem3 = self.twUnidades.horizontalHeaderItem(3)
        ___qtablewidgetitem3.setText(QCoreApplication.translate("frmUnidades", u"Endere\u00e7o", None));
        ___qtablewidgetitem4 = self.twUnidades.horizontalHeaderItem(4)
        ___qtablewidgetitem4.setText(QCoreApplication.translate("frmUnidades", u"UC", None));
        ___qtablewidgetitem5 = self.twUnidades.horizontalHeaderItem(5)
        ___qtablewidgetitem5.setText(QCoreApplication.translate("frmUnidades", u"In\u00edcio Moni.", None));
    # retranslateUi

