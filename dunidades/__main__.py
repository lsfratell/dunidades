import sys
import locale
from PySide2 import QtWidgets, QtGui, QtWebEngine

from dunidades import app_rc
from dunidades.frm_unidades import FrmUnidades

#QtWebEngine.QtWebEngine.initialize()

def criarTabelas():
    from dunidades.modelos import db, Grupo, Unidade, Compensacao, Fatura, Imposto
    with db:
        db.create_tables([Grupo, Unidade, Fatura, Compensacao, Imposto])

def main():
    locale.setlocale(locale.LC_ALL, 'pt_BR.UFT-8')
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle('fusion')
    app.setApplicationDisplayName(f'DUnidades')
    app.setWindowIcon(QtGui.QIcon(':/icon.ico'))
    frmUnidades = FrmUnidades()
    frmUnidades.show()
    sys.exit(app.exec_())

if __name__ == "__main__":
    criarTabelas()
    main()
