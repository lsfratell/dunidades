@echo off
cls
CALL .venv/Scripts/activate
pyside2-uic dunidades/uis/ui_frm_cadastro_fatura.ui -o dunidades/uis/ui_frm_cadastro_fatura.py
pyside2-uic dunidades/uis/ui_frm_cadastro_unidade.ui -o dunidades/uis/ui_frm_cadastro_unidade.py
pyside2-uic dunidades/uis/ui_frm_faturas.ui -o dunidades/uis/ui_frm_faturas.py
pyside2-uic dunidades/uis/ui_frm_grupos.ui -o dunidades/uis/ui_frm_grupos.py
pyside2-uic dunidades/uis/ui_frm_unidades.ui -o dunidades/uis/ui_frm_unidades.py
pyside2-uic dunidades/uis/ui_frm_relatorio.ui -o dunidades/uis/ui_frm_relatorio.py
pyside2-rcc dunidades/app.qrc -o dunidades/app_rc.py
pause
exit